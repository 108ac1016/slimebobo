﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowing : MonoBehaviour
{
    private GameObject Bubble;
    private bool Over;

    private float Save;
    // Start is called before the first frame update
    void Start()
    {
        Bubble = GameObject.FindGameObjectWithTag("Core");
        Over = false;
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Save = Mathf.Abs(transform.position.y - hit.point.y);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!Bubble)
        {
            Over = false;
            Bubble = GameObject.FindGameObjectWithTag("Core");
        }
        else
        {
            Over = (Bubble.transform.position.x > 30);
            if (Over)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(Bubble.transform.position.x, transform.position.y, transform.position.z), Time.deltaTime);
                Ray ray = new Ray(transform.position, Vector3.down);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    var Dis = Vector3.Distance(transform.position, hit.point);
                    if (Dis != Save)
                    {
                        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, hit.point.y + Save, transform.position.z), Time.deltaTime);
                    }
                }
            }
        }
    }
}
