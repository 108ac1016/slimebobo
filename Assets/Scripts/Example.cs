﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    public class example : MonoBehaviour
    {
        public Matrix4x4 originalProjection;
        void Update()
        {
            //改變原始矩陣的某些值
            Matrix4x4 p = originalProjection;
            p.m01 += Mathf.Sin(Time.time * 1.2F) * 0.1F;
            p.m10 += Mathf.Sin(Time.time * 1.5F) * 0.1F;
            Camera.main.projectionMatrix = p;
        }
        public void Awake()
        {
            originalProjection = Camera.main.projectionMatrix;
        }
    }
}
