﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMovement : MonoBehaviour
{
    private Camera Main = Camera.main;
    [Range(0.0f, 1000.0f)]
    public float Distance = 450f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!Main){
            Main = Camera.main;
        }
        else{
            if(this.tag == "Left"){
                var newPos = new Vector3(Main.transform.position.x - Distance, transform.position.y, transform.position.z);
                transform.position = newPos;
            }
            else if(this.tag == "Right"){
                var newPos = new Vector3(Main.transform.position.x + Distance, transform.position.y, transform.position.z);
                transform.position = newPos;
            }
        }
    }
}
