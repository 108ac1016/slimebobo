﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coral_aniamtion : MonoBehaviour
{
    public GameObject[] Corals;
    public void Hit(){
        foreach (var c in Corals){
            c.GetComponent<Animator>().SetTrigger("Hit");
        }
    }
}
