using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeHandlerForChild : MonoBehaviour
{
    [HideInInspector]
    private int direction;
    private bool used = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Slime")
        {
            var collisionPoint = other.ClosestPoint(transform.position);
            Rigidbody R1 = this.GetComponent<Rigidbody>();
            Rigidbody R2 = other.GetComponent<Rigidbody>();
            Transform T1 = this.transform;
            Transform T2 = other.transform;
            var HJ = this.gameObject.AddComponent<HingeJoint>();
            HJ.connectedBody = R2;
            var P1 = (2 * R2.mass) / (R1.mass + R2.mass);
            var P2 = (R1.velocity - R2.velocity) * (T1.localScale.x - T2.localScale.x) / Mathf.Pow((T1.localScale.x - T2.localScale.x), 2);
            var P3 = (T1.localScale.x - T2.localScale.x);
            other.GetComponent<Rigidbody>().AddForce(P1 * P2 * P3);
            Debug.Log("TouchSlime:" + (P1 * P2 * P3));
        }
        else if (other.tag == "Wall" || other.tag == "Floor")
        {
            AudioSource.PlayClipAtPoint(this.GetComponent<AudioSource>().clip, other.ClosestPoint(transform.position));
            this.GetComponent<SkinnedMeshRenderer>().enabled = false;
            Destroy(this.gameObject, this.GetComponent<AudioSource>().clip.length);
        }
        // else if (other.tag == "Floor")
        // {
        //     var collisionPoint = other.ClosestPoint(transform.position);
        //     Rigidbody R1 = this.GetComponent<Rigidbody>();
        //     Transform T1 = this.transform;

        //     Ray ray = new Ray(transform.position, collisionPoint);
        //     RaycastHit hit;
        //     if (Physics.Raycast(ray, out hit))
        //     {
        //         var RF = Vector3.Reflect(T1.position, hit.normal);
        //         this.GetComponent<Rigidbody>().velocity = RF.normalized * R1.velocity.magnitude;
        //         Debug.Log("TouchWall:" + collisionPoint);
        //     }
        // }
        else if (other.tag == "Core")
        {
            used = true;
            AudioSource.PlayClipAtPoint(other.GetComponent<SlimeHandler>().Sounds[Random.Range(0, 1)], other.ClosestPoint(transform.position));
            var collisionPoint = other.ClosestPoint(transform.position);
            Rigidbody R1 = this.GetComponent<Rigidbody>();
            Rigidbody R2 = other.GetComponent<Rigidbody>();
            Transform T1 = this.transform;
            Transform T2 = other.transform;
            // var HJ = this.gameObject.AddComponent<HingeJoint>();
            // HJ.connectedBody = R2;
            var P1 = (2 * R2.mass) / (R1.mass + R2.mass);
            var P2 = (R1.velocity - R2.velocity) * (T1.localScale.x - T2.localScale.x) / Mathf.Pow((T1.localScale.x - T2.localScale.x), 2);
            var P3 = (T1.localScale.x - T2.localScale.x);
            other.GetComponent<Rigidbody>().AddForce(P1 * P2 * P3);
        }
        else if (other.tag == "Mist")
        {
            this.GetComponent<SkinnedMeshRenderer>().enabled = false;
            var Bubbles = GameObject.FindGameObjectsWithTag("Bubble");
            var nearest = 0f;
            var NearBubble = GameObject.FindGameObjectWithTag("Bubble");
            foreach (var b in Bubbles)
            {
                if (Vector3.Distance(transform.position, b.transform.position) < nearest || nearest == 0)
                {
                    nearest = Vector3.Distance(transform.position, b.transform.position);
                    NearBubble = b;
                }
            }
            NearBubble.GetComponent<ParticleSystem>().Play();
            // NearBubble.GetComponent<AudioSource>().Play();
            var Clip = NearBubble.GetComponent<AudioSource>().clip;
            AudioSource.PlayClipAtPoint(Clip, other.ClosestPoint(transform.position));
            Destroy(this.gameObject, Clip.length);
        }
        else if (other.tag == "Egg")
        {
            this.GetComponent<SkinnedMeshRenderer>().enabled = false;
            var Egg = other.GetComponentInParent<Animator>();
            Egg.SetTrigger("Hit");
            // other.GetComponentInParent<AudioSource>().Play();
            var Clip = other.GetComponentInParent<AudioSource>().clip;
            AudioSource.PlayClipAtPoint(Clip, other.ClosestPoint(transform.position));
            other.GetComponentInParent<Egg_animation>().Borne();
            Destroy(this.gameObject, Clip.length);
        }
        else if (other.tag == "Coral")
        {
            this.GetComponent<SkinnedMeshRenderer>().enabled = false;
            var Coral = other.transform.parent.GetComponent<Coral_aniamtion>();
            Coral.Hit();
            // other.transform.parent.GetComponent<AudioSource>().Play();
            var Clip = other.transform.parent.GetComponent<AudioSource>().clip;
            AudioSource.PlayClipAtPoint(Clip, other.ClosestPoint(transform.position));
            Destroy(this.gameObject, Clip.length);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        used = false;
    }

    private void Start()
    {
        direction = Random.Range(1, -1);
        this.GetComponent<Rigidbody>().mass = (4 * Mathf.PI * Mathf.Pow(this.transform.localScale.x, 3)) / (4 * Mathf.PI * 1000);
        // this.GetComponent<AudioSource>().Play();
    }
    private void Update()
    {
        if (!used)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, direction, 0), Time.deltaTime);
            Destroy(this.gameObject, 3);
        }
    }

}
