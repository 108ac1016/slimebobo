using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeHandler : MonoBehaviour
{
    public float AbsorbTime = 10f;
    public float CountTime;
    public float Scale;
    // public ParticleSystem Explode;
    public AudioClip[] Sounds;
    private float CountTimeSave;
    private bool touching = false;
    private float dis;
    private Vector3 ScaleSave;
    private int direction;
    static private int up = 1, down = -1;
    private EventControll EventC;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Slime")
        {
            Debug.Log("Touch");
            other.gameObject.GetComponent<MovementControl>().Stop = true;
            touching = true;
        }
        else if (other.tag == "Wall" || other.tag == "Floor")
        {
            var Slimes = GameObject.FindGameObjectsWithTag("Slime");
            for (int i = 0; i < Slimes.Length; i++)
            {
                Destroy(Slimes[i]);
            }
            var EX = GameObject.FindGameObjectWithTag("Explode").GetComponent<ParticleSystem>();
            EX.transform.position = transform.position;
            EX.Play();
            EventC.Dead = 1;
            EventC.Fade();
            Destroy(this.gameObject);
            
        }
        else if (other.tag == "Fish")
        {
            var Slimes = GameObject.FindGameObjectsWithTag("Slime");
            for (int i = 0; i < Slimes.Length; i++)
            {
                Destroy(Slimes[i]);
            }
            var EX = GameObject.FindGameObjectWithTag("Explode").GetComponent<ParticleSystem>();
            EX.transform.position = transform.position;
            EX.Play();
            EventC.Dead = 2;
            EventC.Fade();
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Slime")
        {
            other.transform.position = Vector3.Lerp(other.transform.position, this.transform.position, Time.deltaTime / AbsorbTime);
            touching = true;
            if (Vector3.Distance(other.transform.position, transform.position) < transform.localScale.x)
            {
                Absorb(other.gameObject);
            }
            else
            {
                Destroy(other.gameObject);
                ScaleSave = this.transform.localScale;
            }
        }
    }
    void Absorb(GameObject Slime)
    {
        transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale + new Vector3(1, 1, 1) * (Slime.transform.localScale.x / transform.localScale.x), Time.deltaTime);
        Slime.transform.localScale = Vector3.Lerp(Slime.transform.localScale, Slime.transform.localScale - new Vector3(1, 1, 1), Time.deltaTime);
        if (transform.localScale.x > 100)
        {
            var Slimes = GameObject.FindGameObjectsWithTag("Slime");
            foreach (GameObject Bubble in Slimes)
            {
                Destroy(Bubble);
            }
            Destroy(this.gameObject);
            EventC.Dead = 1;
            EventC.Fade();

            Debug.Log("Explode");
        }
        Vector3 size = Vector3.right + new Vector3(1, 1, 0) * (Slime.transform.localScale.x / transform.localScale.x);
        Vector3 pos = (new Vector3(-1, 0, 0) * (Slime.transform.position.x - transform.position.x) + new Vector3(0, -1, 0) * (Slime.transform.position.y - transform.position.y)) * (Slime.transform.localScale.x / transform.localScale.x) * Scale;
        transform.position = Vector3.Lerp(transform.position, transform.position + pos, Time.deltaTime);
        Slime.transform.position = Vector3.Lerp(Slime.transform.position, Slime.transform.position + pos, Time.deltaTime);
        if ((transform.localScale.x / Slime.transform.localScale.x) > 9)
        {
            Destroy(Slime);
        }
    }

    void End()
    {
        transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3((820 - transform.position.x), 0, 0), Time.deltaTime);
    }

    void Start()
    {
        CountTimeSave = CountTime;
        direction = Random.Range(-1, 1);
        EventC = GameObject.FindGameObjectWithTag("Player").GetComponent<EventControll>();
    }
    void Update()
    {
        this.GetComponent<Rigidbody>().mass = (4 * Mathf.PI * Mathf.Pow(this.transform.localScale.x, 3)) / (4 * Mathf.PI * 1000);

        if (GameObject.FindGameObjectsWithTag("Slime").Length == 0)
        {
            touching = false;
            direction = Random.Range(-1, 1);
        }
        if (!touching)
        {
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                dis = Vector3.Distance(transform.position, hit.point);
                if (dis < 30)
                {
                    if (direction != up)
                        direction = 1;
                    transform.position = Vector3.Lerp(transform.position, transform.position + Vector3.up, Time.deltaTime);
                }
                else if (dis > 80)
                {
                    if (direction != down)
                        direction = -1;
                    transform.position = Vector3.Lerp(transform.position, transform.position + Vector3.down, Time.deltaTime);
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, transform.position + Vector3.up * direction, Time.deltaTime);
                }
            }
            // Debug.Log("Floating: " + dis);

            if (CountTime < 0)
            {
                transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale - Vector3.one, Time.deltaTime);
                if (transform.localScale.x < 20)
                {
                    EventC.Dead = 1;
                    Destroy(this.gameObject);
                }
            }
            else
            {
                CountTime -= Time.deltaTime;
                // Debug.Log("Time left :" + CountTime);
            }

            if(transform.position.x >= 820){
                End();
            }
        }
        else
        {
            CountTime = CountTimeSave;
        }
    }
}
