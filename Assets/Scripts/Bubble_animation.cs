using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class Bubble_animation : MonoBehaviour
{
    Renderer rend;
    [Range(0.0f, 10.0f)]
    public float Scale = 0f;


    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Slime" || other.tag == "Core")
            // HitPos
            rend.material.SetVector("Vector3_4712A18", other.transform.position);
        // HitScale
        var Dis = Vector3.Distance(transform.position, other.transform.position);
        rend.material.SetFloat("Vector1_1B7E3644", Mathf.Abs(other.transform.localScale.x - Dis) * Scale);
        // DS
        //// rend.material.SetFloat("Vector1_5C383FD9", -(0.6f));
    }

    private void OnTriggerExit(Collider other)
    {
        rend.material.SetVector("Vector3_4712A18", Vector4.zero);
        rend.material.SetFloat("Vector1_1B7E3644", 0);
    }


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

}


// Shader Prop
// HitPos, Vector3_4712A18
// HitScale, Vector1_1B7E3644
// DS, Vector1_5C383FD9

// Distance 20/40/60