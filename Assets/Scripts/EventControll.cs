﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventControll : MonoBehaviour
{
    public GameObject Prefab;
    public Transform[] RespawnPoint;
    public float AnimTime;
    [HideInInspector]
    public int Dead = 1;

    private Vector3 StartPoint;
    private Vector3 StartSize;
    private bool EventActive = false;

    private void Start()
    {
        var first = GameObject.FindGameObjectWithTag("Core");
        StartPoint = first.transform.position;
        StartSize = Prefab.transform.localScale;
    }
    private void Update()
    {
        if (!GameObject.FindGameObjectWithTag("Core"))
        {
            Camera.main.transform.position = new Vector3(0, 50, 10);
            if (Dead % 2 == 0 && !EventActive)
            {
                var KanA = GameObject.FindGameObjectWithTag("Crab").GetComponent<Animator>();

                if (KanA.GetCurrentAnimatorStateInfo(0).IsName("CrabIdle"))
                {
                    KanA.SetBool("Regen", true);
                }
                else
                {
                    if (KanA.GetCurrentAnimatorStateInfo(0).normalizedTime >= .45f)
                    {
                        Instantiate(Prefab, RespawnPoint[Dead % 2].position, transform.rotation);
                        var Main = GameObject.FindWithTag("Core");
                        Main.transform.localScale = new Vector3(1, 1, 1);
                        Main.GetComponent<SlimeHandler>().enabled = false;
                        EventActive = true;
                    }
                }

            }
            else
            {
                Instantiate(Prefab, RespawnPoint[Dead % 2].position, transform.rotation);
                var Main = GameObject.FindWithTag("Core");
                Main.transform.localScale = new Vector3(1, 1, 1);
                Main.GetComponent<SlimeHandler>().enabled = false;
                EventActive = true;
            }
        }
        else
        {
            if (EventActive)
            {
                var KanA = GameObject.FindGameObjectWithTag("Crab").GetComponent<Animator>();
                if (KanA.GetCurrentAnimatorStateInfo(0).IsName("CrabAnim"))
                {
                    KanA.SetBool("Regen", false);
                }

                var Main = GameObject.FindGameObjectWithTag("Core");
                if (Vector3.Distance(Main.transform.position, StartPoint) < 3 && Vector3.Distance(Main.transform.localScale, StartSize) < 3)
                {
                    Main.GetComponent<SlimeHandler>().enabled = true;
                    EventActive = false;
                }
                else
                {
                    Main.transform.position = Vector3.Lerp(Main.transform.position, Main.transform.position + Vector3.Normalize(StartPoint - Main.transform.position), Time.deltaTime / AnimTime);
                    Main.transform.localScale = Vector3.Lerp(Main.transform.localScale, Main.transform.localScale + Vector3.Normalize(StartSize - Main.transform.localScale), Time.deltaTime / AnimTime);
                }
            }
        }

    }

    public void Fade()
    {
        Camera.main.GetComponent<Animator>().SetTrigger("Restart");
    }
}
