﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementControl : MonoBehaviour
{
    [HideInInspector]
    public bool Stop = false;
    [HideInInspector]
    public Vector3 velocitySave;
    // [Range(8.0f, 20.0f)]
    // public float SpeedAddOn = 10f;

    void Update()
    {
        if (Stop)
        {
            velocitySave = this.GetComponent<Rigidbody>().velocity;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else
        {
            if (this.GetComponent<Rigidbody>().velocity != Vector3.zero)
            {
                var RB = this.GetComponent<Rigidbody>();
                // RB.velocity -= Vector3.Lerp(Vector3.zero, RB.velocity, Time.deltaTime);
                RB.velocity = Vector3.Lerp(RB.velocity, RB.velocity - RB.velocity.normalized, Time.deltaTime);
            }
        }
    }

    public void moving(Vector3 path, float speed)
    {
        this.GetComponent<Rigidbody>().AddForce(path * speed);
    }
}
