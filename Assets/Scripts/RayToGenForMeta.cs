using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayToGenForMeta : MonoBehaviour
{
    public OSCManager oscManager;

    // [Range(0.0f, 20.0f)]
    // public float radius = 5.0f;
    // [Range(8.0f, 20.0f)]
    // public float size = 10.0f;
    // [HideInInspector]
    // public int slime_Count = 0;
    private float force = 1f;
    public GameObject slime_Prefab;
    private bool slime_exist = false;
    private Vector3 direction;
    private Vector3 urg_direction;
    private GameObject Slime;

    private bool born;
    // Start is called before the first frame update

    void slime_generation()
    {
        if (!Slime)
        {
            if (born)
            {
                Vector2 direct = new Vector2();
                direct = new Vector2(urg_direction.x * 106.66f,urg_direction.y * 120);

                Ray ray = Camera.main.ScreenPointToRay(direct);

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log(hit.point + " " + hit.distance);
                    direct = hit.point;
                }

                var Slime = Instantiate(slime_Prefab, direct, transform.rotation);
                born = false;
            }


            if (Input.GetMouseButtonDown(0))
            {
                force = 1;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Debug.Log(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    // Debug.Log(hit.point + " " + hit.distance);
                    direction = hit.point;
                }
            }
            else if (Input.GetMouseButton(0))
            {
                force += Time.deltaTime * 70;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                var diff = direction - Camera.main.transform.position;
                var Slime = Instantiate(slime_Prefab, direction, transform.rotation);
                // Debug.Log("Shoot:" + force);
                direction -= Slime.transform.position;
                Slime.GetComponent<MovementControl>().moving(direction, force);
            }

            Slime = null;
        }

    }
    void ball(List<Vector3> list)
    {
        if (!born)
        {
            Vector3 count = new Vector3(0, 0, 0);
            foreach (Vector3 v in list)
            {
                count += v;
            }
            count /= 3;
            urg_direction = count;
            born = true;
        }


    }

    void Start()
    {
        oscManager.addressEvent.Add(new AddressEvent("/urg", ball));

        if (slime_Prefab != null)
            slime_exist = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (slime_exist)
            slime_generation();
        else
        {
            if (slime_Prefab != null)
                slime_exist = true;
        }
    }

}
