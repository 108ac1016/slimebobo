﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace uFlex
{
    public class RayToGenerate : FlexProcessor
    {
        public FlexParticles particles;
        public uint mass = 200;
        public float radius = 5.0f;
        public float speed = 100f;
        private uint id = 0;
        void Start()
        {
            if (particles == null)
                particles = GetComponent<FlexParticles>();
        }
        public override void PreContainerUpdate(FlexSolver solver, FlexContainer cntr, FlexParameters parameters)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log(hit.point);
                    Vector3 velocity = (hit.point - Camera.main.transform.position) / hit.distance * speed;
                    Vector3 pos = hit.point - ((hit.point - Camera.main.transform.position) / radius);
                    for (uint i = id; i < particles.m_particlesCount && i < id + mass; i++)
                    {
                        particles.m_particlesActivity[i] = true;
                        particles.m_particles[i].pos = pos + UnityEngine.Random.insideUnitSphere * radius;
                        particles.m_velocities[i] = velocity;
                    }
                    id += mass;
                    if (id > particles.m_particlesCount - 1)
                    {
                        id = 0;
                    }
                }
            }
        }
    }
}



