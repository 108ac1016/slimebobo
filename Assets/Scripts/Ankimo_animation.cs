﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ankimo_animation : MonoBehaviour
{
    // [HideInInspector]
    public bool Catch;
    public Transform Lamp;
    Animator Anim;
    GameObject Bubble;

    void Start()
    {
        if (!Anim)
        {
            Anim = this.GetComponent<Animator>();
        }
    }
    void Update()
    {
        if (!Bubble)
        {
            Bubble = GameObject.FindGameObjectWithTag("Core");
        }
        else
        {
            if (Bubble.transform.position.x >= 820)
            {
                Catch = true;
            }
            else
            {
                Catch = false;
            }
        }


        Anim.SetBool("Catch", Catch);
    }
}
