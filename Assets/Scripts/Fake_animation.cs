﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fake_animation : MonoBehaviour
{
    public float AnimTime = 1f;
    public int DeadTime = 5;
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, transform.position + Vector3.up, Time.deltaTime / AnimTime);
        transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale + Vector3.one, Time.deltaTime / AnimTime);
        Destroy(this.gameObject, DeadTime);
    }
}
