﻿Shader "PBR Master"
{
    Properties
    {
        Vector1_96E67BC1("Divide Color", Range(0, 3)) = 1
        Vector1_EB297D4("Halo", Range(0, 1)) = 0
        Vector1_8511B5BA("Blend Opacity", Range(0, 1)) = 0.2
        Vector1_AA9EFC36("Blend Sharpness", Range(1, 5)) = 2
        Vector1_BBCB5707("Noise Scale", Range(0, 1)) = 0.6
        Vector1_43DEBB60("Speed", Float) = 0.1
        Vector1_127E9B60("IOR", Float) = 0
        [NoScaleOffset]Cubemap_1521ADFB("Cubemap", CUBE) = "" {}
        [NoScaleOffset]Cubemap_53B24D1("CubemapAlpha", CUBE) = "" {}
    }
    SubShader
    {
        Tags
        {
            "RenderPipeline"="UniversalPipeline"
            "RenderType"="Transparent"
            "Queue"="Transparent+0"
        }
        
        Pass
        {
            Name "Universal Forward"
            Tags 
            { 
                "LightMode" = "UniversalForward"
            }
           
            // Render State
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
            Cull Back
            ZTest LEqual
            ZWrite Off
            // ColorMask: <None>
            
        
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            // Debug
            // <None>
        
            // --------------------------------------------------
            // Pass
        
            // Pragmas
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
            #pragma multi_compile_fog
            #pragma multi_compile_instancing
        
            // Keywords
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
            #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
            // GraphKeywords: <None>
            
            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _NORMAL_DROPOFF_WS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_POSITION_WS 
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
            #define SHADERPASS_FORWARD
            #define REQUIRE_OPAQUE_TEXTURE
        
            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
        
            // --------------------------------------------------
            // Graph
        
            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
            float Vector1_96E67BC1;
            float Vector1_EB297D4;
            float Vector1_8511B5BA;
            float Vector1_AA9EFC36;
            float Vector1_BBCB5707;
            float Vector1_43DEBB60;
            float Vector1_127E9B60;
            CBUFFER_END
            TEXTURECUBE(Cubemap_1521ADFB); SAMPLER(samplerCubemap_1521ADFB);
            TEXTURECUBE(Cubemap_53B24D1); SAMPLER(samplerCubemap_53B24D1);
        
            // Graph Functions
            
            void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
            {
                Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
            }
            
            void Unity_Absolute_float3(float3 In, out float3 Out)
            {
                Out = abs(In);
            }
            
            void Unity_Power_float3(float3 A, float3 B, out float3 Out)
            {
                Out = pow(A, B);
            }
            
            void Unity_Normalize_float3(float3 In, out float3 Out)
            {
                Out = normalize(In);
            }
            
            void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
            {
                Out = A * B;
            }
            
            void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
            {
                Out = UV * Tiling + Offset;
            }
            
            void Unity_Divide_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A / B;
            }
            
            
            float2 Unity_GradientNoise_Dir_float(float2 p)
            {
                // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
                p = p % 289;
                float x = (34 * p.x + 1) * p.x % 289 + p.y;
                x = (34 * x + 1) * x % 289;
                x = frac(x / 41) * 2 - 1;
                return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
            }
            
            void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
            { 
                float2 p = UV * Scale;
                float2 ip = floor(p);
                float2 fp = frac(p);
                float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
                float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
                float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
                float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
                fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
                Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
            }
            
            void Unity_Multiply_float(float A, float B, out float Out)
            {
                Out = A * B;
            }
            
            void Unity_Add_float(float A, float B, out float Out)
            {
                Out = A + B;
            }
            
            void Unity_Subtract_float(float A, float B, out float Out)
            {
                Out = A - B;
            }
            
            void Unity_Clamp_float(float In, float Min, float Max, out float Out)
            {
                Out = clamp(In, Min, Max);
            }
            
            void Unity_SampleGradient_float(Gradient Gradient, float Time, out float4 Out)
            {
                float3 color = Gradient.colors[0].rgb;
                [unroll]
                for (int c = 1; c < 8; c++)
                {
                    float colorPos = saturate((Time - Gradient.colors[c-1].w) / (Gradient.colors[c].w - Gradient.colors[c-1].w)) * step(c, Gradient.colorsLength-1);
                    color = lerp(color, Gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), Gradient.type));
                }
            #ifndef UNITY_COLORSPACE_GAMMA
                color = SRGBToLinear(color);
            #endif
                float alpha = Gradient.alphas[0].x;
                [unroll]
                for (int a = 1; a < 8; a++)
                {
                    float alphaPos = saturate((Time - Gradient.alphas[a-1].y) / (Gradient.alphas[a].y - Gradient.alphas[a-1].y)) * step(a, Gradient.alphasLength-1);
                    alpha = lerp(alpha, Gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), Gradient.type));
                }
                Out = float4(color, alpha);
            }
            
            void Unity_Blend_Lighten_float4(float4 Base, float4 Blend, out float4 Out, float Opacity)
            {
                Out = max(Blend, Base);
                Out = lerp(Base, Out, Opacity);
            }
            
            void ref_float(float3 View, float3 Normal, float IOR, out float3 Out)
            {
                Out = refract(View, Normal, IOR);
            }
            
            void Unity_Add_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A + B;
            }
            
            void Unity_Floor_float3(float3 In, out float3 Out)
            {
                Out = floor(In);
            }
            
            void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A - B;
            }
            
            void Unity_SceneColor_float(float4 UV, out float3 Out)
            {
                Out = SHADERGRAPH_SAMPLE_SCENE_COLOR(UV.xy);
            }
            
            void Unity_ReflectionProbe_float(float3 ViewDir, float3 Normal, float LOD, out float3 Out)
            {
                Out = SHADERGRAPH_REFLECTION_PROBE(ViewDir, Normal, LOD);
            }
            
            void Unity_Divide_float(float A, float B, out float Out)
            {
                Out = A / B;
            }
            
            void Unity_Lerp_float3(float3 A, float3 B, float3 T, out float3 Out)
            {
                Out = lerp(A, B, T);
            }
        
            // Graph Vertex
            // GraphVertex: <None>
            
            // Graph Pixel
            struct SurfaceDescriptionInputs
            {
                float3 WorldSpaceNormal;
                float3 WorldSpaceTangent;
                float3 WorldSpaceBiTangent;
                float3 WorldSpaceViewDirection;
                float3 WorldSpacePosition;
                float3 AbsoluteWorldSpacePosition;
                float4 ScreenPosition;
                float3 TimeParameters;
            };
            
            struct SurfaceDescription
            {
                float3 Albedo;
                float3 Normal;
                float3 Emission;
                float Metallic;
                float Smoothness;
                float Occlusion;
                float Alpha;
                float AlphaClipThreshold;
            };
            
            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
            {
                SurfaceDescription surface = (SurfaceDescription)0;
                Gradient _Gradient_C79F68B0_Out_0 = NewGradient(0, 5, 2, float4(0, 1, 0, 0),float4(0, 1, 1, 0.4),float4(0, 0, 1, 0.5000076),float4(1, 0, 1, 0.6),float4(1, 1, 0, 1),float4(0, 0, 0, 0),float4(0, 0, 0, 0),float4(0, 0, 0, 0), float2(1, 0),float2(1, 1),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0));
                float _FresnelEffect_568339AE_Out_3;
                Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 0.3, _FresnelEffect_568339AE_Out_3);
                float3 _Absolute_137C0BF8_Out_1;
                Unity_Absolute_float3(IN.WorldSpaceNormal, _Absolute_137C0BF8_Out_1);
                float _Property_DAA2A74E_Out_0 = Vector1_AA9EFC36;
                float3 _Power_C21C81E5_Out_2;
                Unity_Power_float3(_Absolute_137C0BF8_Out_1, (_Property_DAA2A74E_Out_0.xxx), _Power_C21C81E5_Out_2);
                float3 _Normalize_EEE3EE45_Out_1;
                Unity_Normalize_float3(_Power_C21C81E5_Out_2, _Normalize_EEE3EE45_Out_1);
                float _Split_5A393B97_R_1 = _Normalize_EEE3EE45_Out_1[0];
                float _Split_5A393B97_G_2 = _Normalize_EEE3EE45_Out_1[1];
                float _Split_5A393B97_B_3 = _Normalize_EEE3EE45_Out_1[2];
                float _Split_5A393B97_A_4 = 0;
                float _Split_4CD00F09_R_1 = IN.AbsoluteWorldSpacePosition[0];
                float _Split_4CD00F09_G_2 = IN.AbsoluteWorldSpacePosition[1];
                float _Split_4CD00F09_B_3 = IN.AbsoluteWorldSpacePosition[2];
                float _Split_4CD00F09_A_4 = 0;
                float2 _Vector2_D8138178_Out_0 = float2(_Split_4CD00F09_G_2, _Split_4CD00F09_B_3);
                float _Property_997DA5DB_Out_0 = Vector1_43DEBB60;
                float3 _Multiply_32CDC3F0_Out_2;
                Unity_Multiply_float((_Property_997DA5DB_Out_0.xxx), float3(length(float3(UNITY_MATRIX_M[0].x, UNITY_MATRIX_M[1].x, UNITY_MATRIX_M[2].x)),
                                         length(float3(UNITY_MATRIX_M[0].y, UNITY_MATRIX_M[1].y, UNITY_MATRIX_M[2].y)),
                                         length(float3(UNITY_MATRIX_M[0].z, UNITY_MATRIX_M[1].z, UNITY_MATRIX_M[2].z))), _Multiply_32CDC3F0_Out_2);
                float3 _Multiply_A192A3FA_Out_2;
                Unity_Multiply_float((IN.TimeParameters.x.xxx), _Multiply_32CDC3F0_Out_2, _Multiply_A192A3FA_Out_2);
                float2 _TilingAndOffset_72BF9796_Out_3;
                Unity_TilingAndOffset_float(_Vector2_D8138178_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_72BF9796_Out_3);
                float Slider_D9A16563 = 4.76;
                float3 _Divide_C2C54E6D_Out_2;
                Unity_Divide_float3((Slider_D9A16563.xxx), float3(length(float3(UNITY_MATRIX_M[0].x, UNITY_MATRIX_M[1].x, UNITY_MATRIX_M[2].x)),
                                         length(float3(UNITY_MATRIX_M[0].y, UNITY_MATRIX_M[1].y, UNITY_MATRIX_M[2].y)),
                                         length(float3(UNITY_MATRIX_M[0].z, UNITY_MATRIX_M[1].z, UNITY_MATRIX_M[2].z))), _Divide_C2C54E6D_Out_2);
                float _GradientNoise_64C8AA5A_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_72BF9796_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_64C8AA5A_Out_2);
                float _Multiply_3512CB71_Out_2;
                Unity_Multiply_float(_Split_5A393B97_R_1, _GradientNoise_64C8AA5A_Out_2, _Multiply_3512CB71_Out_2);
                float2 _Vector2_D43F1723_Out_0 = float2(_Split_4CD00F09_R_1, _Split_4CD00F09_B_3);
                float2 _TilingAndOffset_E902E3AC_Out_3;
                Unity_TilingAndOffset_float(_Vector2_D43F1723_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_E902E3AC_Out_3);
                float _GradientNoise_81B3BBC6_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_E902E3AC_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_81B3BBC6_Out_2);
                float _Multiply_5BEC0A65_Out_2;
                Unity_Multiply_float(_Split_5A393B97_G_2, _GradientNoise_81B3BBC6_Out_2, _Multiply_5BEC0A65_Out_2);
                float _Add_CFA6BEFD_Out_2;
                Unity_Add_float(_Multiply_3512CB71_Out_2, _Multiply_5BEC0A65_Out_2, _Add_CFA6BEFD_Out_2);
                float2 _Vector2_5AB45EEF_Out_0 = float2(_Split_4CD00F09_R_1, _Split_4CD00F09_G_2);
                float2 _TilingAndOffset_CEA1DB8E_Out_3;
                Unity_TilingAndOffset_float(_Vector2_5AB45EEF_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_CEA1DB8E_Out_3);
                float _GradientNoise_9BB5D86A_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_CEA1DB8E_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_9BB5D86A_Out_2);
                float _Multiply_9985B311_Out_2;
                Unity_Multiply_float(_Split_5A393B97_B_3, _GradientNoise_9BB5D86A_Out_2, _Multiply_9985B311_Out_2);
                float _Add_FD540A44_Out_2;
                Unity_Add_float(_Add_CFA6BEFD_Out_2, _Multiply_9985B311_Out_2, _Add_FD540A44_Out_2);
                float _Property_DB9D95F2_Out_0 = Vector1_BBCB5707;
                float _Subtract_1C60D59E_Out_2;
                Unity_Subtract_float(_Add_FD540A44_Out_2, _Property_DB9D95F2_Out_0, _Subtract_1C60D59E_Out_2);
                float _Clamp_2E197EB1_Out_3;
                Unity_Clamp_float(_Subtract_1C60D59E_Out_2, 0, 1, _Clamp_2E197EB1_Out_3);
                float _Subtract_656CCC49_Out_2;
                Unity_Subtract_float(_FresnelEffect_568339AE_Out_3, _Clamp_2E197EB1_Out_3, _Subtract_656CCC49_Out_2);
                float4 _SampleGradient_77E54C33_Out_2;
                Unity_SampleGradient_float(_Gradient_C79F68B0_Out_0, _Subtract_656CCC49_Out_2, _SampleGradient_77E54C33_Out_2);
                float _Property_FC005A1F_Out_0 = Vector1_8511B5BA;
                float4 _Blend_A006ED8D_Out_2;
                Unity_Blend_Lighten_float4(float4(0, 0, 0, 0), _SampleGradient_77E54C33_Out_2, _Blend_A006ED8D_Out_2, _Property_FC005A1F_Out_0);
                float4 _ScreenPosition_3AD4D8B1_Out_0 = float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0);
                float3 _Normalize_A2971188_Out_1;
                Unity_Normalize_float3(IN.WorldSpaceViewDirection, _Normalize_A2971188_Out_1);
                float3 _Normalize_ADCC76C9_Out_1;
                Unity_Normalize_float3(IN.WorldSpaceNormal, _Normalize_ADCC76C9_Out_1);
                float _Property_4AF1DBE7_Out_0 = Vector1_127E9B60;
                float3 _CustomFunction_125D1925_Out_3;
                ref_float(_Normalize_A2971188_Out_1, _Normalize_ADCC76C9_Out_1, _Property_4AF1DBE7_Out_0, _CustomFunction_125D1925_Out_3);
                float3x3 Transform_BD46A402_tangentTransform_World = float3x3(IN.WorldSpaceTangent, IN.WorldSpaceBiTangent, IN.WorldSpaceNormal);
                float3 _Transform_BD46A402_Out_1 = TransformWorldToTangent(_CustomFunction_125D1925_Out_3.xyz, Transform_BD46A402_tangentTransform_World);
                float3 _Add_39D89B5E_Out_2;
                Unity_Add_float3((_ScreenPosition_3AD4D8B1_Out_0.xyz), _Transform_BD46A402_Out_1, _Add_39D89B5E_Out_2);
                float3 _Floor_45272C12_Out_1;
                Unity_Floor_float3(_Add_39D89B5E_Out_2, _Floor_45272C12_Out_1);
                float3 _Subtract_32C2544A_Out_2;
                Unity_Subtract_float3(_Add_39D89B5E_Out_2, _Floor_45272C12_Out_1, _Subtract_32C2544A_Out_2);
                float3 _SceneColor_8AAF6AE5_Out_1;
                Unity_SceneColor_float((float4(_Subtract_32C2544A_Out_2, 1.0)), _SceneColor_8AAF6AE5_Out_1);
                float3 _ReflectionProbe_67C72F9A_Out_3;
                Unity_ReflectionProbe_float(IN.WorldSpaceViewDirection, IN.WorldSpaceNormal, 0, _ReflectionProbe_67C72F9A_Out_3);
                float3 _Multiply_8079B899_Out_2;
                Unity_Multiply_float(IN.WorldSpaceViewDirection, float3(-1, -1, 1), _Multiply_8079B899_Out_2);
                float3 _Multiply_1DEC564_Out_2;
                Unity_Multiply_float(IN.WorldSpaceNormal, float3(-1, -1, 1), _Multiply_1DEC564_Out_2);
                float3 _ReflectionProbe_576EFC93_Out_3;
                Unity_ReflectionProbe_float(_Multiply_8079B899_Out_2, _Multiply_1DEC564_Out_2, 0, _ReflectionProbe_576EFC93_Out_3);
                float3 _Add_AC1E8152_Out_2;
                Unity_Add_float3(_ReflectionProbe_67C72F9A_Out_3, _ReflectionProbe_576EFC93_Out_3, _Add_AC1E8152_Out_2);
                float _Property_F0E6B5B3_Out_0 = Vector1_96E67BC1;
                float3 _Divide_AD88FD35_Out_2;
                Unity_Divide_float3(_Add_AC1E8152_Out_2, (_Property_F0E6B5B3_Out_0.xxx), _Divide_AD88FD35_Out_2);
                float _Property_4C6FA0A8_Out_0 = Vector1_EB297D4;
                float _FresnelEffect_22A6F68F_Out_3;
                Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, _Property_4C6FA0A8_Out_0, _FresnelEffect_22A6F68F_Out_3);
                float _Divide_21E2AE01_Out_2;
                Unity_Divide_float(_FresnelEffect_22A6F68F_Out_3, 2, _Divide_21E2AE01_Out_2);
                float3 _Lerp_C0E16A38_Out_3;
                Unity_Lerp_float3(_SceneColor_8AAF6AE5_Out_1, _Divide_AD88FD35_Out_2, (_Divide_21E2AE01_Out_2.xxx), _Lerp_C0E16A38_Out_3);
                float3 _Lerp_AFCFF706_Out_3;
                Unity_Lerp_float3((_Blend_A006ED8D_Out_2.xyz), _Lerp_C0E16A38_Out_3, (_Divide_21E2AE01_Out_2.xxx), _Lerp_AFCFF706_Out_3);
                surface.Albedo = _Lerp_AFCFF706_Out_3;
                surface.Normal = IN.WorldSpaceNormal;
                surface.Emission = _Lerp_C0E16A38_Out_3;
                surface.Metallic = 0;
                surface.Smoothness = 0.5;
                surface.Occlusion = 1;
                surface.Alpha = 1;
                surface.AlphaClipThreshold = 0;
                return surface;
            }
        
            // --------------------------------------------------
            // Structs and Packing
        
            // Generated Type: Attributes
            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float4 tangentOS : TANGENT;
                float4 uv1 : TEXCOORD1;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : INSTANCEID_SEMANTIC;
                #endif
            };
        
            // Generated Type: Varyings
            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 positionWS;
                float3 normalWS;
                float4 tangentWS;
                float3 viewDirectionWS;
                #if defined(LIGHTMAP_ON)
                float2 lightmapUV;
                #endif
                #if !defined(LIGHTMAP_ON)
                float3 sh;
                #endif
                float4 fogFactorAndVertexLight;
                float4 shadowCoord;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Generated Type: PackedVaryings
            struct PackedVaryings
            {
                float4 positionCS : SV_POSITION;
                #if defined(LIGHTMAP_ON)
                #endif
                #if !defined(LIGHTMAP_ON)
                #endif
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                float3 interp00 : TEXCOORD0;
                float3 interp01 : TEXCOORD1;
                float4 interp02 : TEXCOORD2;
                float3 interp03 : TEXCOORD3;
                float2 interp04 : TEXCOORD4;
                float3 interp05 : TEXCOORD5;
                float4 interp06 : TEXCOORD6;
                float4 interp07 : TEXCOORD7;
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Packed Type: Varyings
            PackedVaryings PackVaryings(Varyings input)
            {
                PackedVaryings output = (PackedVaryings)0;
                output.positionCS = input.positionCS;
                output.interp00.xyz = input.positionWS;
                output.interp01.xyz = input.normalWS;
                output.interp02.xyzw = input.tangentWS;
                output.interp03.xyz = input.viewDirectionWS;
                #if defined(LIGHTMAP_ON)
                output.interp04.xy = input.lightmapUV;
                #endif
                #if !defined(LIGHTMAP_ON)
                output.interp05.xyz = input.sh;
                #endif
                output.interp06.xyzw = input.fogFactorAndVertexLight;
                output.interp07.xyzw = input.shadowCoord;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
            
            // Unpacked Type: Varyings
            Varyings UnpackVaryings(PackedVaryings input)
            {
                Varyings output = (Varyings)0;
                output.positionCS = input.positionCS;
                output.positionWS = input.interp00.xyz;
                output.normalWS = input.interp01.xyz;
                output.tangentWS = input.interp02.xyzw;
                output.viewDirectionWS = input.interp03.xyz;
                #if defined(LIGHTMAP_ON)
                output.lightmapUV = input.interp04.xy;
                #endif
                #if !defined(LIGHTMAP_ON)
                output.sh = input.interp05.xyz;
                #endif
                output.fogFactorAndVertexLight = input.interp06.xyzw;
                output.shadowCoord = input.interp07.xyzw;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
        
            // --------------------------------------------------
            // Build Graph Inputs
        
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
            
            	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
            	float3 unnormalizedNormalWS = input.normalWS;
                const float renormFactor = 1.0 / length(unnormalizedNormalWS);
            
            	// use bitangent on the fly like in hdrp
            	// IMPORTANT! If we ever support Flip on double sided materials ensure bitangent and tangent are NOT flipped.
                float crossSign = (input.tangentWS.w > 0.0 ? 1.0 : -1.0) * GetOddNegativeScale();
            	float3 bitang = crossSign * cross(input.normalWS.xyz, input.tangentWS.xyz);
            
                output.WorldSpaceNormal =            renormFactor*input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
            
            	// to preserve mikktspace compliance we use same scale renormFactor as was used on the normal.
            	// This is explained in section 2.2 in "surface gradient based bump mapping framework"
                output.WorldSpaceTangent =           renormFactor*input.tangentWS.xyz;
            	output.WorldSpaceBiTangent =         renormFactor*bitang;
            
                output.WorldSpaceViewDirection =     input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
                output.WorldSpacePosition =          input.positionWS;
                output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionWS), _ProjectionParams.x);
                output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
            #else
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #endif
            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            
                return output;
            }
            
        
            // --------------------------------------------------
            // Main
        
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"
        
            ENDHLSL
        }
        
        Pass
        {
            Name "ShadowCaster"
            Tags 
            { 
                "LightMode" = "ShadowCaster"
            }
           
            // Render State
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
            Cull Back
            ZTest LEqual
            ZWrite On
            // ColorMask: <None>
            
        
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            // Debug
            // <None>
        
            // --------------------------------------------------
            // Pass
        
            // Pragmas
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
            #pragma multi_compile_instancing
        
            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>
            
            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _NORMAL_DROPOFF_WS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define VARYINGS_NEED_NORMAL_WS
            #define SHADERPASS_SHADOWCASTER
        
            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
        
            // --------------------------------------------------
            // Graph
        
            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
            float Vector1_96E67BC1;
            float Vector1_EB297D4;
            float Vector1_8511B5BA;
            float Vector1_AA9EFC36;
            float Vector1_BBCB5707;
            float Vector1_43DEBB60;
            float Vector1_127E9B60;
            CBUFFER_END
            TEXTURECUBE(Cubemap_1521ADFB); SAMPLER(samplerCubemap_1521ADFB);
            TEXTURECUBE(Cubemap_53B24D1); SAMPLER(samplerCubemap_53B24D1);
        
            // Graph Functions
            // GraphFunctions: <None>
        
            // Graph Vertex
            // GraphVertex: <None>
            
            // Graph Pixel
            struct SurfaceDescriptionInputs
            {
                float3 WorldSpaceNormal;
            };
            
            struct SurfaceDescription
            {
                float Alpha;
                float AlphaClipThreshold;
            };
            
            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
            {
                SurfaceDescription surface = (SurfaceDescription)0;
                surface.Alpha = 1;
                surface.AlphaClipThreshold = 0;
                return surface;
            }
        
            // --------------------------------------------------
            // Structs and Packing
        
            // Generated Type: Attributes
            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float4 tangentOS : TANGENT;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : INSTANCEID_SEMANTIC;
                #endif
            };
        
            // Generated Type: Varyings
            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 normalWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Generated Type: PackedVaryings
            struct PackedVaryings
            {
                float4 positionCS : SV_POSITION;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                float3 interp00 : TEXCOORD0;
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Packed Type: Varyings
            PackedVaryings PackVaryings(Varyings input)
            {
                PackedVaryings output = (PackedVaryings)0;
                output.positionCS = input.positionCS;
                output.interp00.xyz = input.normalWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
            
            // Unpacked Type: Varyings
            Varyings UnpackVaryings(PackedVaryings input)
            {
                Varyings output = (Varyings)0;
                output.positionCS = input.positionCS;
                output.normalWS = input.interp00.xyz;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
        
            // --------------------------------------------------
            // Build Graph Inputs
        
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
            
            	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
            	float3 unnormalizedNormalWS = input.normalWS;
                const float renormFactor = 1.0 / length(unnormalizedNormalWS);
            
            
                output.WorldSpaceNormal =            renormFactor*input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
            
            
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
            #else
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #endif
            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            
                return output;
            }
            
        
            // --------------------------------------------------
            // Main
        
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"
        
            ENDHLSL
        }
        
        Pass
        {
            Name "DepthOnly"
            Tags 
            { 
                "LightMode" = "DepthOnly"
            }
           
            // Render State
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
            Cull Back
            ZTest LEqual
            ZWrite On
            ColorMask 0
            
        
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            // Debug
            // <None>
        
            // --------------------------------------------------
            // Pass
        
            // Pragmas
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
            #pragma multi_compile_instancing
        
            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>
            
            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _NORMAL_DROPOFF_WS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define VARYINGS_NEED_NORMAL_WS
            #define SHADERPASS_DEPTHONLY
        
            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
        
            // --------------------------------------------------
            // Graph
        
            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
            float Vector1_96E67BC1;
            float Vector1_EB297D4;
            float Vector1_8511B5BA;
            float Vector1_AA9EFC36;
            float Vector1_BBCB5707;
            float Vector1_43DEBB60;
            float Vector1_127E9B60;
            CBUFFER_END
            TEXTURECUBE(Cubemap_1521ADFB); SAMPLER(samplerCubemap_1521ADFB);
            TEXTURECUBE(Cubemap_53B24D1); SAMPLER(samplerCubemap_53B24D1);
        
            // Graph Functions
            // GraphFunctions: <None>
        
            // Graph Vertex
            // GraphVertex: <None>
            
            // Graph Pixel
            struct SurfaceDescriptionInputs
            {
                float3 WorldSpaceNormal;
            };
            
            struct SurfaceDescription
            {
                float Alpha;
                float AlphaClipThreshold;
            };
            
            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
            {
                SurfaceDescription surface = (SurfaceDescription)0;
                surface.Alpha = 1;
                surface.AlphaClipThreshold = 0;
                return surface;
            }
        
            // --------------------------------------------------
            // Structs and Packing
        
            // Generated Type: Attributes
            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float4 tangentOS : TANGENT;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : INSTANCEID_SEMANTIC;
                #endif
            };
        
            // Generated Type: Varyings
            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 normalWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Generated Type: PackedVaryings
            struct PackedVaryings
            {
                float4 positionCS : SV_POSITION;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                float3 interp00 : TEXCOORD0;
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Packed Type: Varyings
            PackedVaryings PackVaryings(Varyings input)
            {
                PackedVaryings output = (PackedVaryings)0;
                output.positionCS = input.positionCS;
                output.interp00.xyz = input.normalWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
            
            // Unpacked Type: Varyings
            Varyings UnpackVaryings(PackedVaryings input)
            {
                Varyings output = (Varyings)0;
                output.positionCS = input.positionCS;
                output.normalWS = input.interp00.xyz;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
        
            // --------------------------------------------------
            // Build Graph Inputs
        
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
            
            	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
            	float3 unnormalizedNormalWS = input.normalWS;
                const float renormFactor = 1.0 / length(unnormalizedNormalWS);
            
            
                output.WorldSpaceNormal =            renormFactor*input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
            
            
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
            #else
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #endif
            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            
                return output;
            }
            
        
            // --------------------------------------------------
            // Main
        
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"
        
            ENDHLSL
        }
        
        Pass
        {
            Name "Meta"
            Tags 
            { 
                "LightMode" = "Meta"
            }
           
            // Render State
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
            Cull Back
            ZTest LEqual
            ZWrite On
            // ColorMask: <None>
            
        
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            // Debug
            // <None>
        
            // --------------------------------------------------
            // Pass
        
            // Pragmas
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
        
            // Keywords
            #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            // GraphKeywords: <None>
            
            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _NORMAL_DROPOFF_WS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define ATTRIBUTES_NEED_TEXCOORD2
            #define VARYINGS_NEED_POSITION_WS 
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define SHADERPASS_META
            #define REQUIRE_OPAQUE_TEXTURE
        
            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
        
            // --------------------------------------------------
            // Graph
        
            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
            float Vector1_96E67BC1;
            float Vector1_EB297D4;
            float Vector1_8511B5BA;
            float Vector1_AA9EFC36;
            float Vector1_BBCB5707;
            float Vector1_43DEBB60;
            float Vector1_127E9B60;
            CBUFFER_END
            TEXTURECUBE(Cubemap_1521ADFB); SAMPLER(samplerCubemap_1521ADFB);
            TEXTURECUBE(Cubemap_53B24D1); SAMPLER(samplerCubemap_53B24D1);
        
            // Graph Functions
            
            void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
            {
                Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
            }
            
            void Unity_Absolute_float3(float3 In, out float3 Out)
            {
                Out = abs(In);
            }
            
            void Unity_Power_float3(float3 A, float3 B, out float3 Out)
            {
                Out = pow(A, B);
            }
            
            void Unity_Normalize_float3(float3 In, out float3 Out)
            {
                Out = normalize(In);
            }
            
            void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
            {
                Out = A * B;
            }
            
            void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
            {
                Out = UV * Tiling + Offset;
            }
            
            void Unity_Divide_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A / B;
            }
            
            
            float2 Unity_GradientNoise_Dir_float(float2 p)
            {
                // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
                p = p % 289;
                float x = (34 * p.x + 1) * p.x % 289 + p.y;
                x = (34 * x + 1) * x % 289;
                x = frac(x / 41) * 2 - 1;
                return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
            }
            
            void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
            { 
                float2 p = UV * Scale;
                float2 ip = floor(p);
                float2 fp = frac(p);
                float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
                float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
                float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
                float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
                fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
                Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
            }
            
            void Unity_Multiply_float(float A, float B, out float Out)
            {
                Out = A * B;
            }
            
            void Unity_Add_float(float A, float B, out float Out)
            {
                Out = A + B;
            }
            
            void Unity_Subtract_float(float A, float B, out float Out)
            {
                Out = A - B;
            }
            
            void Unity_Clamp_float(float In, float Min, float Max, out float Out)
            {
                Out = clamp(In, Min, Max);
            }
            
            void Unity_SampleGradient_float(Gradient Gradient, float Time, out float4 Out)
            {
                float3 color = Gradient.colors[0].rgb;
                [unroll]
                for (int c = 1; c < 8; c++)
                {
                    float colorPos = saturate((Time - Gradient.colors[c-1].w) / (Gradient.colors[c].w - Gradient.colors[c-1].w)) * step(c, Gradient.colorsLength-1);
                    color = lerp(color, Gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), Gradient.type));
                }
            #ifndef UNITY_COLORSPACE_GAMMA
                color = SRGBToLinear(color);
            #endif
                float alpha = Gradient.alphas[0].x;
                [unroll]
                for (int a = 1; a < 8; a++)
                {
                    float alphaPos = saturate((Time - Gradient.alphas[a-1].y) / (Gradient.alphas[a].y - Gradient.alphas[a-1].y)) * step(a, Gradient.alphasLength-1);
                    alpha = lerp(alpha, Gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), Gradient.type));
                }
                Out = float4(color, alpha);
            }
            
            void Unity_Blend_Lighten_float4(float4 Base, float4 Blend, out float4 Out, float Opacity)
            {
                Out = max(Blend, Base);
                Out = lerp(Base, Out, Opacity);
            }
            
            void ref_float(float3 View, float3 Normal, float IOR, out float3 Out)
            {
                Out = refract(View, Normal, IOR);
            }
            
            void Unity_Add_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A + B;
            }
            
            void Unity_Floor_float3(float3 In, out float3 Out)
            {
                Out = floor(In);
            }
            
            void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A - B;
            }
            
            void Unity_SceneColor_float(float4 UV, out float3 Out)
            {
                Out = SHADERGRAPH_SAMPLE_SCENE_COLOR(UV.xy);
            }
            
            void Unity_ReflectionProbe_float(float3 ViewDir, float3 Normal, float LOD, out float3 Out)
            {
                Out = SHADERGRAPH_REFLECTION_PROBE(ViewDir, Normal, LOD);
            }
            
            void Unity_Divide_float(float A, float B, out float Out)
            {
                Out = A / B;
            }
            
            void Unity_Lerp_float3(float3 A, float3 B, float3 T, out float3 Out)
            {
                Out = lerp(A, B, T);
            }
        
            // Graph Vertex
            // GraphVertex: <None>
            
            // Graph Pixel
            struct SurfaceDescriptionInputs
            {
                float3 WorldSpaceNormal;
                float3 WorldSpaceTangent;
                float3 WorldSpaceBiTangent;
                float3 WorldSpaceViewDirection;
                float3 WorldSpacePosition;
                float3 AbsoluteWorldSpacePosition;
                float4 ScreenPosition;
                float3 TimeParameters;
            };
            
            struct SurfaceDescription
            {
                float3 Albedo;
                float3 Emission;
                float Alpha;
                float AlphaClipThreshold;
            };
            
            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
            {
                SurfaceDescription surface = (SurfaceDescription)0;
                Gradient _Gradient_C79F68B0_Out_0 = NewGradient(0, 5, 2, float4(0, 1, 0, 0),float4(0, 1, 1, 0.4),float4(0, 0, 1, 0.5000076),float4(1, 0, 1, 0.6),float4(1, 1, 0, 1),float4(0, 0, 0, 0),float4(0, 0, 0, 0),float4(0, 0, 0, 0), float2(1, 0),float2(1, 1),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0));
                float _FresnelEffect_568339AE_Out_3;
                Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 0.3, _FresnelEffect_568339AE_Out_3);
                float3 _Absolute_137C0BF8_Out_1;
                Unity_Absolute_float3(IN.WorldSpaceNormal, _Absolute_137C0BF8_Out_1);
                float _Property_DAA2A74E_Out_0 = Vector1_AA9EFC36;
                float3 _Power_C21C81E5_Out_2;
                Unity_Power_float3(_Absolute_137C0BF8_Out_1, (_Property_DAA2A74E_Out_0.xxx), _Power_C21C81E5_Out_2);
                float3 _Normalize_EEE3EE45_Out_1;
                Unity_Normalize_float3(_Power_C21C81E5_Out_2, _Normalize_EEE3EE45_Out_1);
                float _Split_5A393B97_R_1 = _Normalize_EEE3EE45_Out_1[0];
                float _Split_5A393B97_G_2 = _Normalize_EEE3EE45_Out_1[1];
                float _Split_5A393B97_B_3 = _Normalize_EEE3EE45_Out_1[2];
                float _Split_5A393B97_A_4 = 0;
                float _Split_4CD00F09_R_1 = IN.AbsoluteWorldSpacePosition[0];
                float _Split_4CD00F09_G_2 = IN.AbsoluteWorldSpacePosition[1];
                float _Split_4CD00F09_B_3 = IN.AbsoluteWorldSpacePosition[2];
                float _Split_4CD00F09_A_4 = 0;
                float2 _Vector2_D8138178_Out_0 = float2(_Split_4CD00F09_G_2, _Split_4CD00F09_B_3);
                float _Property_997DA5DB_Out_0 = Vector1_43DEBB60;
                float3 _Multiply_32CDC3F0_Out_2;
                Unity_Multiply_float((_Property_997DA5DB_Out_0.xxx), float3(length(float3(UNITY_MATRIX_M[0].x, UNITY_MATRIX_M[1].x, UNITY_MATRIX_M[2].x)),
                                         length(float3(UNITY_MATRIX_M[0].y, UNITY_MATRIX_M[1].y, UNITY_MATRIX_M[2].y)),
                                         length(float3(UNITY_MATRIX_M[0].z, UNITY_MATRIX_M[1].z, UNITY_MATRIX_M[2].z))), _Multiply_32CDC3F0_Out_2);
                float3 _Multiply_A192A3FA_Out_2;
                Unity_Multiply_float((IN.TimeParameters.x.xxx), _Multiply_32CDC3F0_Out_2, _Multiply_A192A3FA_Out_2);
                float2 _TilingAndOffset_72BF9796_Out_3;
                Unity_TilingAndOffset_float(_Vector2_D8138178_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_72BF9796_Out_3);
                float Slider_D9A16563 = 4.76;
                float3 _Divide_C2C54E6D_Out_2;
                Unity_Divide_float3((Slider_D9A16563.xxx), float3(length(float3(UNITY_MATRIX_M[0].x, UNITY_MATRIX_M[1].x, UNITY_MATRIX_M[2].x)),
                                         length(float3(UNITY_MATRIX_M[0].y, UNITY_MATRIX_M[1].y, UNITY_MATRIX_M[2].y)),
                                         length(float3(UNITY_MATRIX_M[0].z, UNITY_MATRIX_M[1].z, UNITY_MATRIX_M[2].z))), _Divide_C2C54E6D_Out_2);
                float _GradientNoise_64C8AA5A_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_72BF9796_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_64C8AA5A_Out_2);
                float _Multiply_3512CB71_Out_2;
                Unity_Multiply_float(_Split_5A393B97_R_1, _GradientNoise_64C8AA5A_Out_2, _Multiply_3512CB71_Out_2);
                float2 _Vector2_D43F1723_Out_0 = float2(_Split_4CD00F09_R_1, _Split_4CD00F09_B_3);
                float2 _TilingAndOffset_E902E3AC_Out_3;
                Unity_TilingAndOffset_float(_Vector2_D43F1723_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_E902E3AC_Out_3);
                float _GradientNoise_81B3BBC6_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_E902E3AC_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_81B3BBC6_Out_2);
                float _Multiply_5BEC0A65_Out_2;
                Unity_Multiply_float(_Split_5A393B97_G_2, _GradientNoise_81B3BBC6_Out_2, _Multiply_5BEC0A65_Out_2);
                float _Add_CFA6BEFD_Out_2;
                Unity_Add_float(_Multiply_3512CB71_Out_2, _Multiply_5BEC0A65_Out_2, _Add_CFA6BEFD_Out_2);
                float2 _Vector2_5AB45EEF_Out_0 = float2(_Split_4CD00F09_R_1, _Split_4CD00F09_G_2);
                float2 _TilingAndOffset_CEA1DB8E_Out_3;
                Unity_TilingAndOffset_float(_Vector2_5AB45EEF_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_CEA1DB8E_Out_3);
                float _GradientNoise_9BB5D86A_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_CEA1DB8E_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_9BB5D86A_Out_2);
                float _Multiply_9985B311_Out_2;
                Unity_Multiply_float(_Split_5A393B97_B_3, _GradientNoise_9BB5D86A_Out_2, _Multiply_9985B311_Out_2);
                float _Add_FD540A44_Out_2;
                Unity_Add_float(_Add_CFA6BEFD_Out_2, _Multiply_9985B311_Out_2, _Add_FD540A44_Out_2);
                float _Property_DB9D95F2_Out_0 = Vector1_BBCB5707;
                float _Subtract_1C60D59E_Out_2;
                Unity_Subtract_float(_Add_FD540A44_Out_2, _Property_DB9D95F2_Out_0, _Subtract_1C60D59E_Out_2);
                float _Clamp_2E197EB1_Out_3;
                Unity_Clamp_float(_Subtract_1C60D59E_Out_2, 0, 1, _Clamp_2E197EB1_Out_3);
                float _Subtract_656CCC49_Out_2;
                Unity_Subtract_float(_FresnelEffect_568339AE_Out_3, _Clamp_2E197EB1_Out_3, _Subtract_656CCC49_Out_2);
                float4 _SampleGradient_77E54C33_Out_2;
                Unity_SampleGradient_float(_Gradient_C79F68B0_Out_0, _Subtract_656CCC49_Out_2, _SampleGradient_77E54C33_Out_2);
                float _Property_FC005A1F_Out_0 = Vector1_8511B5BA;
                float4 _Blend_A006ED8D_Out_2;
                Unity_Blend_Lighten_float4(float4(0, 0, 0, 0), _SampleGradient_77E54C33_Out_2, _Blend_A006ED8D_Out_2, _Property_FC005A1F_Out_0);
                float4 _ScreenPosition_3AD4D8B1_Out_0 = float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0);
                float3 _Normalize_A2971188_Out_1;
                Unity_Normalize_float3(IN.WorldSpaceViewDirection, _Normalize_A2971188_Out_1);
                float3 _Normalize_ADCC76C9_Out_1;
                Unity_Normalize_float3(IN.WorldSpaceNormal, _Normalize_ADCC76C9_Out_1);
                float _Property_4AF1DBE7_Out_0 = Vector1_127E9B60;
                float3 _CustomFunction_125D1925_Out_3;
                ref_float(_Normalize_A2971188_Out_1, _Normalize_ADCC76C9_Out_1, _Property_4AF1DBE7_Out_0, _CustomFunction_125D1925_Out_3);
                float3x3 Transform_BD46A402_tangentTransform_World = float3x3(IN.WorldSpaceTangent, IN.WorldSpaceBiTangent, IN.WorldSpaceNormal);
                float3 _Transform_BD46A402_Out_1 = TransformWorldToTangent(_CustomFunction_125D1925_Out_3.xyz, Transform_BD46A402_tangentTransform_World);
                float3 _Add_39D89B5E_Out_2;
                Unity_Add_float3((_ScreenPosition_3AD4D8B1_Out_0.xyz), _Transform_BD46A402_Out_1, _Add_39D89B5E_Out_2);
                float3 _Floor_45272C12_Out_1;
                Unity_Floor_float3(_Add_39D89B5E_Out_2, _Floor_45272C12_Out_1);
                float3 _Subtract_32C2544A_Out_2;
                Unity_Subtract_float3(_Add_39D89B5E_Out_2, _Floor_45272C12_Out_1, _Subtract_32C2544A_Out_2);
                float3 _SceneColor_8AAF6AE5_Out_1;
                Unity_SceneColor_float((float4(_Subtract_32C2544A_Out_2, 1.0)), _SceneColor_8AAF6AE5_Out_1);
                float3 _ReflectionProbe_67C72F9A_Out_3;
                Unity_ReflectionProbe_float(IN.WorldSpaceViewDirection, IN.WorldSpaceNormal, 0, _ReflectionProbe_67C72F9A_Out_3);
                float3 _Multiply_8079B899_Out_2;
                Unity_Multiply_float(IN.WorldSpaceViewDirection, float3(-1, -1, 1), _Multiply_8079B899_Out_2);
                float3 _Multiply_1DEC564_Out_2;
                Unity_Multiply_float(IN.WorldSpaceNormal, float3(-1, -1, 1), _Multiply_1DEC564_Out_2);
                float3 _ReflectionProbe_576EFC93_Out_3;
                Unity_ReflectionProbe_float(_Multiply_8079B899_Out_2, _Multiply_1DEC564_Out_2, 0, _ReflectionProbe_576EFC93_Out_3);
                float3 _Add_AC1E8152_Out_2;
                Unity_Add_float3(_ReflectionProbe_67C72F9A_Out_3, _ReflectionProbe_576EFC93_Out_3, _Add_AC1E8152_Out_2);
                float _Property_F0E6B5B3_Out_0 = Vector1_96E67BC1;
                float3 _Divide_AD88FD35_Out_2;
                Unity_Divide_float3(_Add_AC1E8152_Out_2, (_Property_F0E6B5B3_Out_0.xxx), _Divide_AD88FD35_Out_2);
                float _Property_4C6FA0A8_Out_0 = Vector1_EB297D4;
                float _FresnelEffect_22A6F68F_Out_3;
                Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, _Property_4C6FA0A8_Out_0, _FresnelEffect_22A6F68F_Out_3);
                float _Divide_21E2AE01_Out_2;
                Unity_Divide_float(_FresnelEffect_22A6F68F_Out_3, 2, _Divide_21E2AE01_Out_2);
                float3 _Lerp_C0E16A38_Out_3;
                Unity_Lerp_float3(_SceneColor_8AAF6AE5_Out_1, _Divide_AD88FD35_Out_2, (_Divide_21E2AE01_Out_2.xxx), _Lerp_C0E16A38_Out_3);
                float3 _Lerp_AFCFF706_Out_3;
                Unity_Lerp_float3((_Blend_A006ED8D_Out_2.xyz), _Lerp_C0E16A38_Out_3, (_Divide_21E2AE01_Out_2.xxx), _Lerp_AFCFF706_Out_3);
                surface.Albedo = _Lerp_AFCFF706_Out_3;
                surface.Emission = _Lerp_C0E16A38_Out_3;
                surface.Alpha = 1;
                surface.AlphaClipThreshold = 0;
                return surface;
            }
        
            // --------------------------------------------------
            // Structs and Packing
        
            // Generated Type: Attributes
            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float4 tangentOS : TANGENT;
                float4 uv1 : TEXCOORD1;
                float4 uv2 : TEXCOORD2;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : INSTANCEID_SEMANTIC;
                #endif
            };
        
            // Generated Type: Varyings
            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 positionWS;
                float3 normalWS;
                float4 tangentWS;
                float3 viewDirectionWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Generated Type: PackedVaryings
            struct PackedVaryings
            {
                float4 positionCS : SV_POSITION;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                float3 interp00 : TEXCOORD0;
                float3 interp01 : TEXCOORD1;
                float4 interp02 : TEXCOORD2;
                float3 interp03 : TEXCOORD3;
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Packed Type: Varyings
            PackedVaryings PackVaryings(Varyings input)
            {
                PackedVaryings output = (PackedVaryings)0;
                output.positionCS = input.positionCS;
                output.interp00.xyz = input.positionWS;
                output.interp01.xyz = input.normalWS;
                output.interp02.xyzw = input.tangentWS;
                output.interp03.xyz = input.viewDirectionWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
            
            // Unpacked Type: Varyings
            Varyings UnpackVaryings(PackedVaryings input)
            {
                Varyings output = (Varyings)0;
                output.positionCS = input.positionCS;
                output.positionWS = input.interp00.xyz;
                output.normalWS = input.interp01.xyz;
                output.tangentWS = input.interp02.xyzw;
                output.viewDirectionWS = input.interp03.xyz;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
        
            // --------------------------------------------------
            // Build Graph Inputs
        
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
            
            	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
            	float3 unnormalizedNormalWS = input.normalWS;
                const float renormFactor = 1.0 / length(unnormalizedNormalWS);
            
            	// use bitangent on the fly like in hdrp
            	// IMPORTANT! If we ever support Flip on double sided materials ensure bitangent and tangent are NOT flipped.
                float crossSign = (input.tangentWS.w > 0.0 ? 1.0 : -1.0) * GetOddNegativeScale();
            	float3 bitang = crossSign * cross(input.normalWS.xyz, input.tangentWS.xyz);
            
                output.WorldSpaceNormal =            renormFactor*input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
            
            	// to preserve mikktspace compliance we use same scale renormFactor as was used on the normal.
            	// This is explained in section 2.2 in "surface gradient based bump mapping framework"
                output.WorldSpaceTangent =           renormFactor*input.tangentWS.xyz;
            	output.WorldSpaceBiTangent =         renormFactor*bitang;
            
                output.WorldSpaceViewDirection =     input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
                output.WorldSpacePosition =          input.positionWS;
                output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionWS), _ProjectionParams.x);
                output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
            #else
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #endif
            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            
                return output;
            }
            
        
            // --------------------------------------------------
            // Main
        
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"
        
            ENDHLSL
        }
        
        Pass
        {
            // Name: <None>
            Tags 
            { 
                "LightMode" = "Universal2D"
            }
           
            // Render State
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
            Cull Back
            ZTest LEqual
            ZWrite Off
            // ColorMask: <None>
            
        
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            // Debug
            // <None>
        
            // --------------------------------------------------
            // Pass
        
            // Pragmas
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
            #pragma multi_compile_instancing
        
            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>
            
            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _NORMAL_DROPOFF_WS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define VARYINGS_NEED_POSITION_WS 
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define SHADERPASS_2D
            #define REQUIRE_OPAQUE_TEXTURE
        
            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"
        
            // --------------------------------------------------
            // Graph
        
            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
            float Vector1_96E67BC1;
            float Vector1_EB297D4;
            float Vector1_8511B5BA;
            float Vector1_AA9EFC36;
            float Vector1_BBCB5707;
            float Vector1_43DEBB60;
            float Vector1_127E9B60;
            CBUFFER_END
            TEXTURECUBE(Cubemap_1521ADFB); SAMPLER(samplerCubemap_1521ADFB);
            TEXTURECUBE(Cubemap_53B24D1); SAMPLER(samplerCubemap_53B24D1);
        
            // Graph Functions
            
            void Unity_FresnelEffect_float(float3 Normal, float3 ViewDir, float Power, out float Out)
            {
                Out = pow((1.0 - saturate(dot(normalize(Normal), normalize(ViewDir)))), Power);
            }
            
            void Unity_Absolute_float3(float3 In, out float3 Out)
            {
                Out = abs(In);
            }
            
            void Unity_Power_float3(float3 A, float3 B, out float3 Out)
            {
                Out = pow(A, B);
            }
            
            void Unity_Normalize_float3(float3 In, out float3 Out)
            {
                Out = normalize(In);
            }
            
            void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
            {
                Out = A * B;
            }
            
            void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
            {
                Out = UV * Tiling + Offset;
            }
            
            void Unity_Divide_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A / B;
            }
            
            
            float2 Unity_GradientNoise_Dir_float(float2 p)
            {
                // Permutation and hashing used in webgl-nosie goo.gl/pX7HtC
                p = p % 289;
                float x = (34 * p.x + 1) * p.x % 289 + p.y;
                x = (34 * x + 1) * x % 289;
                x = frac(x / 41) * 2 - 1;
                return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
            }
            
            void Unity_GradientNoise_float(float2 UV, float Scale, out float Out)
            { 
                float2 p = UV * Scale;
                float2 ip = floor(p);
                float2 fp = frac(p);
                float d00 = dot(Unity_GradientNoise_Dir_float(ip), fp);
                float d01 = dot(Unity_GradientNoise_Dir_float(ip + float2(0, 1)), fp - float2(0, 1));
                float d10 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 0)), fp - float2(1, 0));
                float d11 = dot(Unity_GradientNoise_Dir_float(ip + float2(1, 1)), fp - float2(1, 1));
                fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
                Out = lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x) + 0.5;
            }
            
            void Unity_Multiply_float(float A, float B, out float Out)
            {
                Out = A * B;
            }
            
            void Unity_Add_float(float A, float B, out float Out)
            {
                Out = A + B;
            }
            
            void Unity_Subtract_float(float A, float B, out float Out)
            {
                Out = A - B;
            }
            
            void Unity_Clamp_float(float In, float Min, float Max, out float Out)
            {
                Out = clamp(In, Min, Max);
            }
            
            void Unity_SampleGradient_float(Gradient Gradient, float Time, out float4 Out)
            {
                float3 color = Gradient.colors[0].rgb;
                [unroll]
                for (int c = 1; c < 8; c++)
                {
                    float colorPos = saturate((Time - Gradient.colors[c-1].w) / (Gradient.colors[c].w - Gradient.colors[c-1].w)) * step(c, Gradient.colorsLength-1);
                    color = lerp(color, Gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), Gradient.type));
                }
            #ifndef UNITY_COLORSPACE_GAMMA
                color = SRGBToLinear(color);
            #endif
                float alpha = Gradient.alphas[0].x;
                [unroll]
                for (int a = 1; a < 8; a++)
                {
                    float alphaPos = saturate((Time - Gradient.alphas[a-1].y) / (Gradient.alphas[a].y - Gradient.alphas[a-1].y)) * step(a, Gradient.alphasLength-1);
                    alpha = lerp(alpha, Gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), Gradient.type));
                }
                Out = float4(color, alpha);
            }
            
            void Unity_Blend_Lighten_float4(float4 Base, float4 Blend, out float4 Out, float Opacity)
            {
                Out = max(Blend, Base);
                Out = lerp(Base, Out, Opacity);
            }
            
            void ref_float(float3 View, float3 Normal, float IOR, out float3 Out)
            {
                Out = refract(View, Normal, IOR);
            }
            
            void Unity_Add_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A + B;
            }
            
            void Unity_Floor_float3(float3 In, out float3 Out)
            {
                Out = floor(In);
            }
            
            void Unity_Subtract_float3(float3 A, float3 B, out float3 Out)
            {
                Out = A - B;
            }
            
            void Unity_SceneColor_float(float4 UV, out float3 Out)
            {
                Out = SHADERGRAPH_SAMPLE_SCENE_COLOR(UV.xy);
            }
            
            void Unity_ReflectionProbe_float(float3 ViewDir, float3 Normal, float LOD, out float3 Out)
            {
                Out = SHADERGRAPH_REFLECTION_PROBE(ViewDir, Normal, LOD);
            }
            
            void Unity_Divide_float(float A, float B, out float Out)
            {
                Out = A / B;
            }
            
            void Unity_Lerp_float3(float3 A, float3 B, float3 T, out float3 Out)
            {
                Out = lerp(A, B, T);
            }
        
            // Graph Vertex
            // GraphVertex: <None>
            
            // Graph Pixel
            struct SurfaceDescriptionInputs
            {
                float3 WorldSpaceNormal;
                float3 WorldSpaceTangent;
                float3 WorldSpaceBiTangent;
                float3 WorldSpaceViewDirection;
                float3 WorldSpacePosition;
                float3 AbsoluteWorldSpacePosition;
                float4 ScreenPosition;
                float3 TimeParameters;
            };
            
            struct SurfaceDescription
            {
                float3 Albedo;
                float Alpha;
                float AlphaClipThreshold;
            };
            
            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
            {
                SurfaceDescription surface = (SurfaceDescription)0;
                Gradient _Gradient_C79F68B0_Out_0 = NewGradient(0, 5, 2, float4(0, 1, 0, 0),float4(0, 1, 1, 0.4),float4(0, 0, 1, 0.5000076),float4(1, 0, 1, 0.6),float4(1, 1, 0, 1),float4(0, 0, 0, 0),float4(0, 0, 0, 0),float4(0, 0, 0, 0), float2(1, 0),float2(1, 1),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0),float2(0, 0));
                float _FresnelEffect_568339AE_Out_3;
                Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, 0.3, _FresnelEffect_568339AE_Out_3);
                float3 _Absolute_137C0BF8_Out_1;
                Unity_Absolute_float3(IN.WorldSpaceNormal, _Absolute_137C0BF8_Out_1);
                float _Property_DAA2A74E_Out_0 = Vector1_AA9EFC36;
                float3 _Power_C21C81E5_Out_2;
                Unity_Power_float3(_Absolute_137C0BF8_Out_1, (_Property_DAA2A74E_Out_0.xxx), _Power_C21C81E5_Out_2);
                float3 _Normalize_EEE3EE45_Out_1;
                Unity_Normalize_float3(_Power_C21C81E5_Out_2, _Normalize_EEE3EE45_Out_1);
                float _Split_5A393B97_R_1 = _Normalize_EEE3EE45_Out_1[0];
                float _Split_5A393B97_G_2 = _Normalize_EEE3EE45_Out_1[1];
                float _Split_5A393B97_B_3 = _Normalize_EEE3EE45_Out_1[2];
                float _Split_5A393B97_A_4 = 0;
                float _Split_4CD00F09_R_1 = IN.AbsoluteWorldSpacePosition[0];
                float _Split_4CD00F09_G_2 = IN.AbsoluteWorldSpacePosition[1];
                float _Split_4CD00F09_B_3 = IN.AbsoluteWorldSpacePosition[2];
                float _Split_4CD00F09_A_4 = 0;
                float2 _Vector2_D8138178_Out_0 = float2(_Split_4CD00F09_G_2, _Split_4CD00F09_B_3);
                float _Property_997DA5DB_Out_0 = Vector1_43DEBB60;
                float3 _Multiply_32CDC3F0_Out_2;
                Unity_Multiply_float((_Property_997DA5DB_Out_0.xxx), float3(length(float3(UNITY_MATRIX_M[0].x, UNITY_MATRIX_M[1].x, UNITY_MATRIX_M[2].x)),
                                         length(float3(UNITY_MATRIX_M[0].y, UNITY_MATRIX_M[1].y, UNITY_MATRIX_M[2].y)),
                                         length(float3(UNITY_MATRIX_M[0].z, UNITY_MATRIX_M[1].z, UNITY_MATRIX_M[2].z))), _Multiply_32CDC3F0_Out_2);
                float3 _Multiply_A192A3FA_Out_2;
                Unity_Multiply_float((IN.TimeParameters.x.xxx), _Multiply_32CDC3F0_Out_2, _Multiply_A192A3FA_Out_2);
                float2 _TilingAndOffset_72BF9796_Out_3;
                Unity_TilingAndOffset_float(_Vector2_D8138178_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_72BF9796_Out_3);
                float Slider_D9A16563 = 4.76;
                float3 _Divide_C2C54E6D_Out_2;
                Unity_Divide_float3((Slider_D9A16563.xxx), float3(length(float3(UNITY_MATRIX_M[0].x, UNITY_MATRIX_M[1].x, UNITY_MATRIX_M[2].x)),
                                         length(float3(UNITY_MATRIX_M[0].y, UNITY_MATRIX_M[1].y, UNITY_MATRIX_M[2].y)),
                                         length(float3(UNITY_MATRIX_M[0].z, UNITY_MATRIX_M[1].z, UNITY_MATRIX_M[2].z))), _Divide_C2C54E6D_Out_2);
                float _GradientNoise_64C8AA5A_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_72BF9796_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_64C8AA5A_Out_2);
                float _Multiply_3512CB71_Out_2;
                Unity_Multiply_float(_Split_5A393B97_R_1, _GradientNoise_64C8AA5A_Out_2, _Multiply_3512CB71_Out_2);
                float2 _Vector2_D43F1723_Out_0 = float2(_Split_4CD00F09_R_1, _Split_4CD00F09_B_3);
                float2 _TilingAndOffset_E902E3AC_Out_3;
                Unity_TilingAndOffset_float(_Vector2_D43F1723_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_E902E3AC_Out_3);
                float _GradientNoise_81B3BBC6_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_E902E3AC_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_81B3BBC6_Out_2);
                float _Multiply_5BEC0A65_Out_2;
                Unity_Multiply_float(_Split_5A393B97_G_2, _GradientNoise_81B3BBC6_Out_2, _Multiply_5BEC0A65_Out_2);
                float _Add_CFA6BEFD_Out_2;
                Unity_Add_float(_Multiply_3512CB71_Out_2, _Multiply_5BEC0A65_Out_2, _Add_CFA6BEFD_Out_2);
                float2 _Vector2_5AB45EEF_Out_0 = float2(_Split_4CD00F09_R_1, _Split_4CD00F09_G_2);
                float2 _TilingAndOffset_CEA1DB8E_Out_3;
                Unity_TilingAndOffset_float(_Vector2_5AB45EEF_Out_0, float2 (1, 1), (_Multiply_A192A3FA_Out_2.xy), _TilingAndOffset_CEA1DB8E_Out_3);
                float _GradientNoise_9BB5D86A_Out_2;
                Unity_GradientNoise_float(_TilingAndOffset_CEA1DB8E_Out_3, (_Divide_C2C54E6D_Out_2).x, _GradientNoise_9BB5D86A_Out_2);
                float _Multiply_9985B311_Out_2;
                Unity_Multiply_float(_Split_5A393B97_B_3, _GradientNoise_9BB5D86A_Out_2, _Multiply_9985B311_Out_2);
                float _Add_FD540A44_Out_2;
                Unity_Add_float(_Add_CFA6BEFD_Out_2, _Multiply_9985B311_Out_2, _Add_FD540A44_Out_2);
                float _Property_DB9D95F2_Out_0 = Vector1_BBCB5707;
                float _Subtract_1C60D59E_Out_2;
                Unity_Subtract_float(_Add_FD540A44_Out_2, _Property_DB9D95F2_Out_0, _Subtract_1C60D59E_Out_2);
                float _Clamp_2E197EB1_Out_3;
                Unity_Clamp_float(_Subtract_1C60D59E_Out_2, 0, 1, _Clamp_2E197EB1_Out_3);
                float _Subtract_656CCC49_Out_2;
                Unity_Subtract_float(_FresnelEffect_568339AE_Out_3, _Clamp_2E197EB1_Out_3, _Subtract_656CCC49_Out_2);
                float4 _SampleGradient_77E54C33_Out_2;
                Unity_SampleGradient_float(_Gradient_C79F68B0_Out_0, _Subtract_656CCC49_Out_2, _SampleGradient_77E54C33_Out_2);
                float _Property_FC005A1F_Out_0 = Vector1_8511B5BA;
                float4 _Blend_A006ED8D_Out_2;
                Unity_Blend_Lighten_float4(float4(0, 0, 0, 0), _SampleGradient_77E54C33_Out_2, _Blend_A006ED8D_Out_2, _Property_FC005A1F_Out_0);
                float4 _ScreenPosition_3AD4D8B1_Out_0 = float4(IN.ScreenPosition.xy / IN.ScreenPosition.w, 0, 0);
                float3 _Normalize_A2971188_Out_1;
                Unity_Normalize_float3(IN.WorldSpaceViewDirection, _Normalize_A2971188_Out_1);
                float3 _Normalize_ADCC76C9_Out_1;
                Unity_Normalize_float3(IN.WorldSpaceNormal, _Normalize_ADCC76C9_Out_1);
                float _Property_4AF1DBE7_Out_0 = Vector1_127E9B60;
                float3 _CustomFunction_125D1925_Out_3;
                ref_float(_Normalize_A2971188_Out_1, _Normalize_ADCC76C9_Out_1, _Property_4AF1DBE7_Out_0, _CustomFunction_125D1925_Out_3);
                float3x3 Transform_BD46A402_tangentTransform_World = float3x3(IN.WorldSpaceTangent, IN.WorldSpaceBiTangent, IN.WorldSpaceNormal);
                float3 _Transform_BD46A402_Out_1 = TransformWorldToTangent(_CustomFunction_125D1925_Out_3.xyz, Transform_BD46A402_tangentTransform_World);
                float3 _Add_39D89B5E_Out_2;
                Unity_Add_float3((_ScreenPosition_3AD4D8B1_Out_0.xyz), _Transform_BD46A402_Out_1, _Add_39D89B5E_Out_2);
                float3 _Floor_45272C12_Out_1;
                Unity_Floor_float3(_Add_39D89B5E_Out_2, _Floor_45272C12_Out_1);
                float3 _Subtract_32C2544A_Out_2;
                Unity_Subtract_float3(_Add_39D89B5E_Out_2, _Floor_45272C12_Out_1, _Subtract_32C2544A_Out_2);
                float3 _SceneColor_8AAF6AE5_Out_1;
                Unity_SceneColor_float((float4(_Subtract_32C2544A_Out_2, 1.0)), _SceneColor_8AAF6AE5_Out_1);
                float3 _ReflectionProbe_67C72F9A_Out_3;
                Unity_ReflectionProbe_float(IN.WorldSpaceViewDirection, IN.WorldSpaceNormal, 0, _ReflectionProbe_67C72F9A_Out_3);
                float3 _Multiply_8079B899_Out_2;
                Unity_Multiply_float(IN.WorldSpaceViewDirection, float3(-1, -1, 1), _Multiply_8079B899_Out_2);
                float3 _Multiply_1DEC564_Out_2;
                Unity_Multiply_float(IN.WorldSpaceNormal, float3(-1, -1, 1), _Multiply_1DEC564_Out_2);
                float3 _ReflectionProbe_576EFC93_Out_3;
                Unity_ReflectionProbe_float(_Multiply_8079B899_Out_2, _Multiply_1DEC564_Out_2, 0, _ReflectionProbe_576EFC93_Out_3);
                float3 _Add_AC1E8152_Out_2;
                Unity_Add_float3(_ReflectionProbe_67C72F9A_Out_3, _ReflectionProbe_576EFC93_Out_3, _Add_AC1E8152_Out_2);
                float _Property_F0E6B5B3_Out_0 = Vector1_96E67BC1;
                float3 _Divide_AD88FD35_Out_2;
                Unity_Divide_float3(_Add_AC1E8152_Out_2, (_Property_F0E6B5B3_Out_0.xxx), _Divide_AD88FD35_Out_2);
                float _Property_4C6FA0A8_Out_0 = Vector1_EB297D4;
                float _FresnelEffect_22A6F68F_Out_3;
                Unity_FresnelEffect_float(IN.WorldSpaceNormal, IN.WorldSpaceViewDirection, _Property_4C6FA0A8_Out_0, _FresnelEffect_22A6F68F_Out_3);
                float _Divide_21E2AE01_Out_2;
                Unity_Divide_float(_FresnelEffect_22A6F68F_Out_3, 2, _Divide_21E2AE01_Out_2);
                float3 _Lerp_C0E16A38_Out_3;
                Unity_Lerp_float3(_SceneColor_8AAF6AE5_Out_1, _Divide_AD88FD35_Out_2, (_Divide_21E2AE01_Out_2.xxx), _Lerp_C0E16A38_Out_3);
                float3 _Lerp_AFCFF706_Out_3;
                Unity_Lerp_float3((_Blend_A006ED8D_Out_2.xyz), _Lerp_C0E16A38_Out_3, (_Divide_21E2AE01_Out_2.xxx), _Lerp_AFCFF706_Out_3);
                surface.Albedo = _Lerp_AFCFF706_Out_3;
                surface.Alpha = 1;
                surface.AlphaClipThreshold = 0;
                return surface;
            }
        
            // --------------------------------------------------
            // Structs and Packing
        
            // Generated Type: Attributes
            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float4 tangentOS : TANGENT;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : INSTANCEID_SEMANTIC;
                #endif
            };
        
            // Generated Type: Varyings
            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 positionWS;
                float3 normalWS;
                float4 tangentWS;
                float3 viewDirectionWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Generated Type: PackedVaryings
            struct PackedVaryings
            {
                float4 positionCS : SV_POSITION;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                float3 interp00 : TEXCOORD0;
                float3 interp01 : TEXCOORD1;
                float4 interp02 : TEXCOORD2;
                float3 interp03 : TEXCOORD3;
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };
            
            // Packed Type: Varyings
            PackedVaryings PackVaryings(Varyings input)
            {
                PackedVaryings output = (PackedVaryings)0;
                output.positionCS = input.positionCS;
                output.interp00.xyz = input.positionWS;
                output.interp01.xyz = input.normalWS;
                output.interp02.xyzw = input.tangentWS;
                output.interp03.xyz = input.viewDirectionWS;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
            
            // Unpacked Type: Varyings
            Varyings UnpackVaryings(PackedVaryings input)
            {
                Varyings output = (Varyings)0;
                output.positionCS = input.positionCS;
                output.positionWS = input.interp00.xyz;
                output.normalWS = input.interp01.xyz;
                output.tangentWS = input.interp02.xyzw;
                output.viewDirectionWS = input.interp03.xyz;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }
        
            // --------------------------------------------------
            // Build Graph Inputs
        
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);
            
            	// must use interpolated tangent, bitangent and normal before they are normalized in the pixel shader.
            	float3 unnormalizedNormalWS = input.normalWS;
                const float renormFactor = 1.0 / length(unnormalizedNormalWS);
            
            	// use bitangent on the fly like in hdrp
            	// IMPORTANT! If we ever support Flip on double sided materials ensure bitangent and tangent are NOT flipped.
                float crossSign = (input.tangentWS.w > 0.0 ? 1.0 : -1.0) * GetOddNegativeScale();
            	float3 bitang = crossSign * cross(input.normalWS.xyz, input.tangentWS.xyz);
            
                output.WorldSpaceNormal =            renormFactor*input.normalWS.xyz;		// we want a unit length Normal Vector node in shader graph
            
            	// to preserve mikktspace compliance we use same scale renormFactor as was used on the normal.
            	// This is explained in section 2.2 in "surface gradient based bump mapping framework"
                output.WorldSpaceTangent =           renormFactor*input.tangentWS.xyz;
            	output.WorldSpaceBiTangent =         renormFactor*bitang;
            
                output.WorldSpaceViewDirection =     input.viewDirectionWS; //TODO: by default normalized in HD, but not in universal
                output.WorldSpacePosition =          input.positionWS;
                output.AbsoluteWorldSpacePosition =  GetAbsolutePositionWS(input.positionWS);
                output.ScreenPosition =              ComputeScreenPos(TransformWorldToHClip(input.positionWS), _ProjectionParams.x);
                output.TimeParameters =              _TimeParameters.xyz; // This is mainly for LW as HD overwrite this value
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
            #else
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #endif
            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            
                return output;
            }
            
        
            // --------------------------------------------------
            // Main
        
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"
        
            ENDHLSL
        }
        
    }
    CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
    FallBack "Hidden/Shader Graph/FallbackError"
}
