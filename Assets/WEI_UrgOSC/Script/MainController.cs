﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainController : MonoBehaviour
{
    public OSCManager oscManager;

    [SerializeField]
    public GameObject BallPrefab;
    // Start is called before the first frame update
    void Start()
    {
        oscManager.addressEvent.Add(new AddressEvent("/urg", placeBalls));
    }

    void placeBalls(List<Vector3> list){
        GameObject[] oldBalls = GameObject.FindGameObjectsWithTag("ball");
        if (oldBalls != null)
        {
            foreach (GameObject s in oldBalls)
                Destroy(s);
        }
        foreach(Vector3 v in list){
            _ = Instantiate(BallPrefab, v, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
