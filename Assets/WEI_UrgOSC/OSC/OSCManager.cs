﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Osc), typeof(UDPPacketIO))]
public class OSCManager : MonoBehaviour
{
    private Osc oscHandler;
    private Queue<OscMessage> receivedQueue = new Queue<OscMessage>();

    [Header("UDP Setting")]
    public string remoteIp = "127.0.0.1";
    public int sendToPort = 8888;
    public int listenerPort = 12000;

    [Header("Receiver")]
    public List<AddressEvent> addressEvent = new List<AddressEvent>();

    #region Send Test
#if UNITY_EDITOR
    [Header("Send Test")]
    [ContextMenuItem("Send", "Send")]
    [Tooltip("Example:/value 0 \nRight click this filed to invoke Send method.")]
    [SerializeField]
    private string message;

    private void Send()
    {
        if (!Application.isPlaying)
        {
            Debug.LogWarning("這個功能僅能在編輯器Play時操作");
        }
        else
        {
            OSCSend(message);
        }
    }
#endif
    #endregion

    public string Address { get; set; }
    public event Action<string> OnReceive;
    public event Action<string> OnSend;

    private void Start()
    {
        Initial();
    }

    #region Receiver
    private void Update()
    {
        while (receivedQueue.Count > 0)
        {
            var dequeue = receivedQueue.Dequeue();

            OnReceive?.Invoke(ParseToString(dequeue));
            UpdateAddressEvent(dequeue);
        }
    }
    public void AllMessageHandler(OscMessage message)
    {
        // 不能把UnityEvent放在這個Handler裡面Invoke，因為UnityEvent必須在MainThread裡執行，所以要把所有UnityEvent拉到Update裡面
        receivedQueue.Enqueue(message);
    }
    private void UpdateAddressEvent(OscMessage oscMessage)
    {
        foreach (var receiver in addressEvent)
        {
            if (oscMessage.Address == receiver.address)
            {
                receiver.onReceivedNewMessage.Invoke(oscMessage.Values);
            }
        }
    }


    #endregion

    #region Sender
    /// <summary>
    /// 送出包含Address的OSC訊號
    /// </summary>
    /// <param name="content"></param>
    public void OSCSend(List<Vector3> list)
    {
        string text = string.IsNullOrWhiteSpace(Address) ? "" : Address + " ";
        Debug.Log(list.Count.ToString());
        foreach (Vector3 v in list)
        {
            text = text + " " + v.ToString();
        }

		OscMessage msg = Osc.StringToOscMessage(text);
        oscHandler.Send(msg);

        OnSend?.Invoke(ParseToString(msg));

        Address = "";
    }
    public void OSCSend(string content)
    {
        string text = string.IsNullOrWhiteSpace(Address) ? content : Address + " " + content;

        OscMessage msg = Osc.StringToOscMessage(text);
        oscHandler.Send(msg);

        OnSend?.Invoke(text);

        Address = "";
    }
    public void OSCSend(OscMessage message)
    {
        oscHandler.Send(message);

        OnSend?.Invoke(ParseToString(message));
    }
    #endregion

    #region SettingAPI
    [ContextMenu("Reconnect")]
    public void Reconnect()
    {
        Cancel();
        Initial();
    }

    public void SetIP(string ip)
    {
        remoteIp = ip;
        Reconnect();
    }
    public void SetSendPort(string port)
    {
        sendToPort = int.Parse(port);
        Reconnect();
    }
    public void SetListenPort(string port)
    {
        listenerPort = int.Parse(port);
        Reconnect();
    }

    public void Initial()
    {
        if (oscHandler != null) return;

        UDPPacketIO udp = GetComponent<UDPPacketIO>();
        udp.init(remoteIp, sendToPort, listenerPort);

        oscHandler = GetComponent<Osc>();
        oscHandler.init(udp);

        oscHandler.SetAllMessageHandler(AllMessageHandler);
    }
    public void Cancel()
    {
        if (oscHandler == null) return;

        oscHandler.Cancel();
        oscHandler = null;
    }
    #endregion

    ~OSCManager()
    {
        if (oscHandler != null)
        {
            oscHandler.Cancel();
        }

        // speed up finalization
        oscHandler = null;
        GC.Collect();
    }

    private void OnDisable()
    {
        Debug.Log("closing OSC UDP socket in OnDisable");
        oscHandler.Cancel();
        oscHandler = null;
    }

    private string ParseToString(OscMessage message)
    {
        return $"{message.Address} {ArrayListToString(message.Values)}";
    }
    private string ArrayListToString(ArrayList arrayList)
    {
        string text = "";

        for (int i = 0; i < arrayList.Count; i++)
        {
            text += arrayList[i].ToString();

            if (i != arrayList.Count - 1)
            {
                text += ",";
            }
        }
        return text;
    }
}

[Serializable]
public class AddressEvent
{
    [Serializable]
    public class StringEvent : UnityEvent<ArrayList> { }

    public string address;
    public StringEvent onReceivedNewMessage = new StringEvent();

    public AddressEvent(string address, Action<ArrayList> callback)
    {
        this.address = address;
        onReceivedNewMessage.AddListener(a => callback(a));
    }

    public AddressEvent(string address, Action<List<Vector3>> callback)
    {
        this.address = address;
        onReceivedNewMessage.AddListener(a => {
			List<Vector3> vlist = new List<Vector3>();
			for(int i = 0; i < a.Count; i++){
				vlist.Add(StringToVector3(a[i].ToString()));
			}
			callback(vlist);
		});
    }

	public Vector3 StringToVector3(string sVector)
     {
         // Remove the parentheses
         if (sVector.StartsWith ("(") && sVector.EndsWith (")")) {
             sVector = sVector.Substring(1, sVector.Length-2);
         }
 
         // split the items
         string[] sArray = sVector.Split(',');
 
         // store as a Vector3
         Vector3 result = new Vector3(
             float.Parse(sArray[0]),
             float.Parse(sArray[1]),
             float.Parse(sArray[2]));
 
         return result;
     }
}

