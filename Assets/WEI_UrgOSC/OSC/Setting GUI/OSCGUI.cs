﻿using UnityEngine;
using UnityEngine.UI;

public class OSCGUI : MonoBehaviour
{
    public OSCManager manager;
    public InputField IP;
    public InputField sendPort;
    public InputField listenPort;
    public InputField sendTest;
    public Button btnSend;
    public StringStream stream;
    public bool logTime;
    public bool autoSave;
    public string autoSaveSuffix;

    public string autoSave_PrefName_IP => "OSCIP" + autoSaveSuffix;
    public string autoSave_PrefName_SendPort => "OSCSend" + autoSaveSuffix;
    public string autoSave_PrefName_ListenPort => "OSCListen" + autoSaveSuffix;

    private void OnValidate()
    {
        if (!manager && FindObjectOfType<OSCManager>() != null)
            manager = FindObjectOfType<OSCManager>();
    }

    private void Reset()
    {
        autoSaveSuffix = autoSaveSuffix ?? GetInstanceID().ToString();
    }

    public void Start()
    {
        manager.OnSend += Send;
        manager.OnReceive += Receive;

        if (autoSave)
        {
            var prefs_ip = PlayerPrefs.GetString(autoSave_PrefName_IP);
            var prefs_send = PlayerPrefs.GetString(autoSave_PrefName_SendPort);
            var prefs_listen = PlayerPrefs.GetString(autoSave_PrefName_ListenPort);

            if (string.IsNullOrEmpty(prefs_ip))
                PlayerPrefs.SetString(autoSave_PrefName_IP, manager.remoteIp);
            else
                manager.SetIP(prefs_ip);

            if (string.IsNullOrEmpty(prefs_send))
                PlayerPrefs.SetString(autoSave_PrefName_SendPort, manager.sendToPort.ToString());
            else
                manager.SetSendPort(prefs_send);

            if (string.IsNullOrEmpty(prefs_listen))
                PlayerPrefs.SetString(autoSave_PrefName_ListenPort, manager.listenerPort.ToString());
            else
                manager.SetListenPort(prefs_listen);

            if (IP)
                IP.onEndEdit.AddListener((ip) => PlayerPrefs.SetString(autoSave_PrefName_IP, ip));

            if (sendPort)
                sendPort.onEndEdit.AddListener((port) => PlayerPrefs.SetString(autoSave_PrefName_SendPort, port));

            if (listenPort)
                listenPort.onEndEdit.AddListener((port) => PlayerPrefs.SetString(autoSave_PrefName_ListenPort, port));
        }

        if (IP)
        {
            IP.text = manager.remoteIp;
            IP.onEndEdit.AddListener(manager.SetIP);
        }

        if (sendPort)
        {
            sendPort.text = manager.sendToPort.ToString();
            sendPort.onEndEdit.AddListener(manager.SetSendPort);
        }

        if (listenPort)
        {
            listenPort.text = manager.listenerPort.ToString();
            listenPort.onEndEdit.AddListener(manager.SetListenPort);
        }

        if (btnSend)
        {
            btnSend.onClick.AddListener(() => manager.OSCSend(sendTest.text));
        }

        if (stream)
        {
            stream.streamText.text = "Send/Receive Nothing...";
        }
    }

    private void Send(string msg)
    {
        string text = (logTime ? $"(Time: {Time.time.ToString("0.00")})" : "") + " > " + msg;
        stream.Add(text);
    }

    private void Receive(string msg)
    {
        string text = (logTime ? $"(Time: {Time.time.ToString("0.00")})" : "") + " < " + msg;
        stream.Add(text);
    }
}
