﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class StringStream : MonoBehaviour
{
	public Text streamText;
	//public TMP_Text TMPStreamText;
	public int lineCount;

	[HideInInspector]
	public List<string> stream;
	[HideInInspector]
	public string display;

#if UNITY_EDITOR
    private string origin;

    [ContextMenu("Fill 20 Line")]
    private void FillLineCount()
    {
        origin = streamText.text;
        streamText.text = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20";
    }

    [ContextMenu("Recover")]
    private void RecoverLineCount()
    {
        streamText.text = origin;
        EditorUtility.SetDirty(this);
    }
#endif

    public void Add(string msg)
	{
		stream = AsQueue(stream, lineCount, msg);
		display = "";

		foreach (string text in stream)
		{
			display += text.ToString() + "\n";
		}

		streamText.text = display;
		//TMPStreamText.text = display;
	}

	List<T> AsQueue<T>(List<T> list, int size, T type)
	{
		if (list.Count < size)
		{
			list.Add(type);
		}
		else
		{
			list.Remove(list[0]);
			list.Add(type);
		}
		return list;
	}
}
