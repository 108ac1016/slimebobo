# 大三上互動設計專題


##  專題名稱：史萊姆波波

#### 組員: 廖祐凜、徐牧凡、陳紹恩、黃承君、彭裔甄

### 裝置說明:

將球投擲於投影畫面上，
透過雷達眼偵測球體位置，
並在遊戲內生成氣泡來跟遊戲內的物件互動。

### 技術說明：

* Unity 2019.4.10
* 雷達眼
* 投影機

### 遊戲畫面 
##### 點擊圖片播放

[![](https://gitlab.com/108ac1016/slimebobo/-/raw/main/Recordings/screenshot_1.jpg)](https://gitlab.com/108ac1016/slimebobo/-/blob/main/Recordings/%E5%B1%95%E5%A0%B4%E6%94%9D%E5%BD%B1.mp4)

![image](https://gitlab.com/108ac1016/slimebobo/-/raw/main/Recordings/screenshot_2.jpg)
